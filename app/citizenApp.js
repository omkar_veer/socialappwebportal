
    'use strict';

    angular.module('citizenApp',
                          [
                           'utilModule'
                          ])

     .config(['$stateProvider', '$urlRouterProvider','$translateProvider', '$provide','$httpProvider',
      function($stateProvider, $urlRouterProvider,$translateProvider, $provide,$httpProvider) {
            
            $httpProvider.defaults.timeout = 10000;
            $httpProvider.defaults.headers.common = {};
            $httpProvider.defaults.headers.post = {};
            $httpProvider.defaults.headers.put = {};
            $httpProvider.defaults.headers.patch = {};
           
            $stateProvider

              .state('registration', {
                  url: "/registration?cid",
                  controller: 'citizenRegiCtrl',
                  templateUrl: "../app/components/citizenManager/registration/registration.html",
                  cache:false
                  
              })

              .state('main', {
                  url: "/main?cid",
                  controller: 'mainCitizenCtrl',
                  templateUrl: "../app/components/citizenManager/citizenmain.html",
                  cache:false
                 
              })

              .state('home', {
                  url: "/home",
                  //controller: 'citizenHomeCtrl',
                  templateUrl: "../app/components/citizenManager/home/citizenhome.html",
                  cache:false,
                  resolve: {
                     isAuthorised: function(){
                        return (window.sessionStorage.getItem('CTID')) ? true :false;
                      }
                  }
              })

              .state('raisecomplaints', {
                  url: "/raise-complaints",
                  controller: 'complaintManagerCtrl',
                  templateUrl: "../app/components/citizenManager/home/raisecomplaints.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CTID')) ? true :false;
                      }
                  }
              })

              .state('mycomplaints', {
                  url: "/my-complaints",
                  controller: 'complaintListCtrl',
                  templateUrl: "../app/components/citizenManager/home/mycomplaints.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CTID')) ? true :false;
                      }
                  }
              })

              .state('error', {
                  url: "/error",
                  templateUrl: "../app/shared/error/error.html",
              })

              
              // if none of the above states are matched, use this as the fallback
              $urlRouterProvider.otherwise('/error');


               $translateProvider.translations('en',en);
               $translateProvider.translations('mr',mr);

               $translateProvider.preferredLanguage("mr");
               $translateProvider.fallbackLanguage("en");

               // use the HTML5 History API
               //$locationProvider.html5Mode(true);

               // Enable escaping of HTML
              //$translateProvider.useSanitizeValueStrategy('sanitize');


              


        }]).

        run(['$rootScope',function($rootScope){

        }]);
