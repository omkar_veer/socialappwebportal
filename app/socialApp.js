//;(function (window, undefined) {
    'use strict';

    //var socialApp = socialApp || {};

    //socialApp.services = angular.module('socialApp.services', []);
    //socialApp.controllers = angular.module('socialApp.controllers', []);
    //socialApp.directives = angular.module('socialApp.directives', []);

    

    angular.module('socialApp',['utilModule','ui.calendar','ngImgCrop'])

    .config(['$stateProvider', '$urlRouterProvider','$translateProvider', '$provide','$httpProvider',
      function($stateProvider, $urlRouterProvider,$translateProvider, $provide,$httpProvider) {
            
            $httpProvider.defaults.timeout = 10000;
            $httpProvider.defaults.headers.common = {};
            $httpProvider.defaults.headers.post = {};
            $httpProvider.defaults.headers.put = {};
            $httpProvider.defaults.headers.patch = {};
           
            $stateProvider

              .state('login', {
                  url: "/login",
                  controller: 'loginCtrl',
                  templateUrl: "./app/components/login/login.html",
                  cache:false
              })

              .state('dashboard', {
                  url: "/dashboard",
                  controller: 'dashboardCtrl',
                  templateUrl: "./app/components/dashboard/superadmindashboard.html",
                  cache:false,
                  resolve: {
                     isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                     }
                  }
              })

              .state('createcandidate', {
                  url: "/create-candidate",
                  controller: 'manageCandidateCtrl',
                  templateUrl: "./app/components/candidateManager/create-candidate/createcandidate.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                      }
                  }
              })

              .state('editcandidate', {
                  url: "/edit-candidate?cid",
                  controller: 'manageCandidateCtrl',
                  templateUrl: "./app/components/candidateManager/create-candidate/createcandidate.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                      }
                  }
              })

              .state('editprofile', {
                  url: "/edit-profile?cid",
                  controller: 'manageCandidateCtrl',
                  templateUrl: "./app/components/candidateManager/create-candidate/editprofile.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('viewprofile', {
                  url: "/view-profile?cid",
                  controller: 'manageCandidateCtrl',
                  templateUrl: "./app/components/candidateManager/view-candidate/myprofile.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('viewcandidate', {
                  url: "/view-candidate?cid",
                  controller: 'manageCandidateCtrl',
                  templateUrl: "./app/components/candidateManager/view-candidate/viewcandidate.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                      }
                  }
              })

              .state('candidates', {
                  url: "/candidates",
                  controller: 'candidateListCtrl',
                  templateUrl: "./app/components/candidateManager/candidate-list/candidatelist.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                      }
                  }
              })

              .state('import', {
                  url: "/import",
                  controller: 'manageImportCsvCtrl',
                  templateUrl: "./app/components/importcsv/importcsv.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('SSID')) ? true :false;
                      }
                  }
              })

              .state('complaints', {
                  url: "/complaints",
                  controller: 'manageComplaintsCtrl',
                  templateUrl: "./app/components/complaints/complaintlist.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })




              .state('updatecomplaint', {
                  url: "/update-complaint?complaintid",
                  controller: 'manageComplaintsCtrl',
                  templateUrl: "./app/components/complaints/updatecomplaint.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                         return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('announcement', {
                  url: "/announcement",
                  controller: 'manageAnnouncementCtrl',
                  templateUrl: "./app/components/announcement/announcement.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('eventofweek', {
                  url: "/eventofweek",
                  controller: 'manageEventOfWeekCtrl',
                  templateUrl: "./app/components/eventOfWeek/eventofweek.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              //Admin Panel Start
              .state('candidatedashboard', {
                  url: "/admin-dashboard",
                  controller: 'dashboardCtrl',
                  templateUrl: "./app/components/dashboard/candidatedashboard.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('addmanager', {
                  url: "/add-manager?cid",
                  controller: 'addManagerCtrl',
                  templateUrl: "./app/components/candidateManager/add-manager/addmanager.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('addmanagerutility', {
                  url: "/add-manager-utility?cid",
                  controller: 'addManagerCtrl',
                  templateUrl: "./app/components/candidateManager/add-manager/addmanager_birthdayUtility.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('birthdayapp', {
                  url: "/birthday-app?cid",
                  controller: 'birthdayModuleManagerCtrl',
                  templateUrl: "./app/components/candidateManager/birthday-manager/birthdaymodule.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              .state('birthdayOnly', {
                  url: "/birthday-manager?cid",
                  controller: 'birthdayModuleManagerCtrl',
                  templateUrl: "./app/components/candidateManager/birthday-manager/birthdaymoduleonly.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CID')) ? true :false;
                      }
                  }
              })

              
              // if none of the above states are matched, use this as the fallback
              $urlRouterProvider.otherwise('/login');


               $translateProvider.translations('en',en);
               $translateProvider.translations('mr',mr);

               $translateProvider.preferredLanguage("mr");
               $translateProvider.fallbackLanguage("en");

               // use the HTML5 History API
               //$locationProvider.html5Mode(true);

               // Enable escaping of HTML
              //$translateProvider.useSanitizeValueStrategy('sanitize');


              


        }]).

        run(['$rootScope',function($rootScope){
            $rootScope.$on('$stateChangeError',
             function (event, toState, toParams, fromState, fromParams, error) {
               console.log('$stateChangeError', event, toState, toParams, fromState, fromParams, error);
            });
        }]);


       
    

        //window.socialApp = socialApp || {};





//})(window);










