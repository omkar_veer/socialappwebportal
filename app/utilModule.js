 'use strict';
angular.module('utilModule',
  [
         'ui.router',
         'pascalprecht.translate',
         'ui.bootstrap',
         'ngSanitize',
         'ngMessages',
         '720kb.datepicker',
         'ngFileUpload',
         'angularSpinner',
         'ngCsv',
         'angularUtils.directives.dirPagination',
         'jkuri.gallery'
 
]);

var consoleHolder = console;
function debug(bool){
    if(!bool){
        consoleHolder = console;
        console = {};
        console.log = function(){};
    }else
        console = consoleHolder;
}
debug(false); //false for disabling console.log


function showGlobalMenu(){
    $( ".global-menu" ).fadeIn( "slow", function() {});
}

function hideGlobalMenu(){
    $( ".global-menu" ).hide();
}

function loadImageFileAsURL(filesSelected,cb){
    var fileToLoad = filesSelected;
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent){
        return cb(fileLoadedEvent.target.result);
    };
    fileReader.readAsDataURL(fileToLoad);
}

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};

Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function addGlobalEventHandler(){
    setTimeout(function(){
        $('input,textarea').off().on('keypress',function(){
           return  rejectSpecialChar();
        });
    },1000);
}

function rejectSpecialChar(e){
    var key;
    var keychar;
    if (window.event) {
        key = window.event.keyCode
    } else if (e) {
        key = e.which
    } else {
        return true
    }
    keychar = String.fromCharCode(key);
    if(key == 34 || key == 39){
        return false;
    }else{
        return true;
    }

}


function numbersonly(e, decimal) {
    var key;
    var keychar;
    if (window.event) {
        key = window.event.keyCode
    } else if (e) {
        key = e.which
    } else {
        return true
    }
    keychar = String.fromCharCode(key);
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27) || (key == 41) || (key == 40) || (key == 43) || (key == 32)) {
        return true
    } else if ((("0123456789").indexOf(keychar) > -1)) {
        return true
    } else if (decimal && (keychar == ".")) {
        return true
    } else return false
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness  
    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

     





