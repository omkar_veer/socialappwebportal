/***
* Gulp Task Runner file for concate anf minify js/css files for Production evnvironment
* Author: Prashant Patil 
* Created Date :10-May-2016
*
**/


var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var uglifycss = require('gulp-uglifycss');


//Concate& minify All Libs 
gulp.task('commonLibs', function() {
    return gulp.src(['../../assets/libs/angular.min.js',
                     '../../assets/libs/jquery.min.js',
                    '../../assets/libs/ui-bootstrap-tpls-1.1.2.min.js',
                    '../../assets/libs/underscore-min.js',
                    '../../assets/libs/angular-ui-router.min.js',
                    '../../assets/libs/angular-sanitize.min.js',
                    '../../assets/libs/angular-messages.js',
                    '../../assets/libs/angular-translate.min.js',
                    '../../assets/libs/angular-datepicker.js',
                    '../../assets/libs/ng-file-upload-all.js',
                    '../../assets/libs/dirPagination.js',
                    '../../assets/libs/ng-csv.min.js',
                    '../../assets/libs/modernizr.custom.js',
                    '../../assets/libs/spin.min.js',
                    '../../assets/libs/angular-spinner.min.js',
                    '../../assets/libs/cbpFWTabs.js',
                    '../../assets/libs/moment.min.js',
                    '../../assets/libs/calendar.js',
                    '../../assets/libs/fullcalendar.js',
                    '../../assets/libs/gcal.js',
                    '../../assets/libs/jquery-ui.js',
                    '../../assets/libs/ngGallery.js',
                    '../../assets/libs/ng-img-crop.js'
                    
        ])
        .pipe(concat('libs_ns_main-v1.0.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('superAdmin_ns', function() {
    return gulp.src(['../CONSTANTS.js',
                    '../utilModule.js',
                     '../shared/utility/utilityService.js',
                     '../shared/utility/utilityDirectives.js',
                     '../shared/utility/utilityCtrl.js',
                     '../shared/utility/mainCtrl.js',
                     '../shared/popups/OTPmodalPopupCtrl.js',
                     '../shared/popups/modalPopupCtrl.js',
                     '../shared/popups/nsForgotPassPopupCtrl.js',
                     '../shared/popups/UpdatePassPopupCtrl.js',
                     '../socialApp.js',
                     '../components/login/loginCtrl.js',
                     '../components/dashboard/dashboardCtrl.js',
                     '../components/candidateManager/candidate-list/candidateListCtrl.js',
                     '../components/candidateManager/manageCandidateCtrl.js',
                     '../components/candidateManager/add-manager/addManagerCtrl.js',
                     '../components/candidateManager/birthday-manager/birthdayModuleManagerCtrl.js',
                     '../components/complaints/manageComplaintsCtrl.js',
                     '../components/eventOfWeek/manageEventOfWeekCtrl.js',
                     '../components/announcement/manageAnnouncementCtrl.js',
                     '../components/importcsv/manageImportCsvCtrl.js',
                     

                    
        ])
        .pipe(concat('ns_custom.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('karykarta_commonLibs', function() {
    return gulp.src(['../../assets/libs/angular.min.js',
                     '../../assets/libs/jquery.min.js',
                    '../../assets/libs/ui-bootstrap-tpls-1.1.2.min.js',
                    '../../assets/libs/underscore-min.js',
                    '../../assets/libs/angular-ui-router.min.js',
                    '../../assets/libs/angular-sanitize.min.js',
                    '../../assets/libs/angular-messages.js',
                    '../../assets/libs/angular-translate.min.js',
                    '../../assets/libs/angular-datepicker.js',
                    '../../assets/libs/ng-file-upload-all.js',
                    '../../assets/libs/dirPagination.js',
                    '../../assets/libs/ng-csv.min.js',
                    '../../assets/libs/modernizr.custom.js',
                    '../../assets/libs/spin.min.js',
                    '../../assets/libs/angular-spinner.min.js',
                    '../../assets/libs/ngGallery.js'
        ])
        .pipe(concat('libs_nskaryakarta_main-v1.0.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('karyakarta_ns', function() {
    return gulp.src(['../CONSTANTS.js',
                    '../utilModule.js',
                     '../shared/utility/utilityService.js',
                     '../shared/utility/utilityDirectives.js',
                     '../shared/utility/utilityCtrl.js',
                     '../shared/utility/mainCtrl.js',
                     '../shared/popups/OTPmodalPopupCtrl.js',
                     '../shared/popups/modalPopupCtrl.js',
                     '../shared/popups/nsForgotPassPopupCtrl.js',
                     '../shared/popups/UpdatePassPopupCtrl.js',
                     '../karyakartaApp.js',
                     '../components/karyakartaManager/login/karyakartaLoginCtrl.js',
                     '../components/karyakartaManager/add-citizen/addCitizenCtrl.js'
                     
        ])
        .pipe(concat('nskaryakarta_custom.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('citizen_commonLibs', function() {
    return gulp.src(['../../assets/libs/angular.min.js',
                     '../../assets/libs/jquery.min.js',
                    '../../assets/libs/ui-bootstrap-tpls-1.1.2.min.js',
                    '../../assets/libs/underscore-min.js',
                    '../../assets/libs/angular-ui-router.min.js',
                    '../../assets/libs/angular-sanitize.min.js',
                    '../../assets/libs/angular-messages.js',
                    '../../assets/libs/angular-translate.min.js',
                    '../../assets/libs/angular-datepicker.js',
                    '../../assets/libs/ng-file-upload-all.js',
                    '../../assets/libs/dirPagination.js',
                    '../../assets/libs/ng-csv.min.js',
                    '../../assets/libs/modernizr.custom.js',
                    '../../assets/libs/spin.min.js',
                    '../../assets/libs/angular-spinner.min.js',
                    '../../assets/libs/ngGallery.js'
        ])
        .pipe(concat('libs_nscitizen_main-v1.0.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('citizen_ns', function() {
    return gulp.src(['../CONSTANTS.js',
                     '../utilModule.js',
                     '../shared/utility/utilityService.js',
                     '../shared/utility/utilityDirectives.js',
                     '../shared/utility/utilityCtrl.js',
                     '../shared/utility/mainCtrl.js',
                     '../shared/popups/OTPmodalPopupCtrl.js',
                     '../shared/popups/modalPopupCtrl.js',
                     '../shared/popups/nsForgotPassPopupCtrl.js',
                     '../shared/popups/UpdatePassPopupCtrl.js',
                     '../citizenApp.js',
                     '../components/citizenManager/mainCitizenCtrl.js',
                     '../components/citizenManager/home/complaintManagerCtrl.js',
                     '../components/citizenManager/home/complaintListCtrl.js'

                     
        ])
        .pipe(concat('nscitizen_custom.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

 // Default Task
gulp.task('default', ['commonLibs','superAdmin_ns','karykarta_commonLibs','karyakarta_ns',
                      'citizen_commonLibs','citizen_ns'
]);
