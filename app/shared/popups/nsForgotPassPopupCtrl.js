    'use strict';

    angular.module('utilModule').controller('nsForgotPassPopupCtrl',['$scope','$uibModalInstance','$rootScope','utilityService',
        function($scope, $uibModalInstance,$rootScope,utilityService) {
             
        $scope.forgotControl = {};
        console.log("$scope.$parent.usertype",$scope.$parent.userrole); 

        $scope.SendPassword = function () {
           try{
                    utilityService.showWaitLoader();
                    var alertMethodName = 'showAlertMessage';
                    if($scope.$parent.userrole == 'citizen'){
                        alertMethodName = 'showCitizenAlertMessage'
                    }
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.FORGOT_PASS;
                    var postData = {
                                    "phone" : $scope.forgotControl.phone,
                                    "userrole" : $scope.$parent.userrole 
                                };

                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        utilityService.hideWaitLoader();
                        if(typeof data == 'string'){
                                var splitedStr = data.split('{') //[1];
                                if(splitedStr.length != 0){
                                        //splitedStr[1];
                                        var strRes = JSON.parse(splitedStr[1].insert(0, "{"));
                                        $uibModalInstance.close();
                                        if(strRes.status == 'OK'){
                                            utilityService[alertMethodName](strRes.message);
                                        }else{
                                            utilityService[alertMethodName](strRes.message);
                                         }

                                }else{
                                    utilityService[alertMethodName]("Something went wrong please try again");
                                }
                        }else{
                            utilityService[alertMethodName](data.message);
                        }

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService[alertMethodName]("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

        

        $scope.cancelForgotModal = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);






