    'use strict';

    angular.module('utilModule').controller('UpdatePassPopupCtrl',['$scope','$uibModalInstance','$rootScope','utilityService',
        function($scope, $uibModalInstance,$rootScope,utilityService) {
             
        $scope.updatePassControl = {};
        console.log("$scope.$parent.userrole",$scope.$parent.userrole); 
        $scope.isButtonClicked = false;

        $scope.ChangePassword = function (form) {
           try{
                $scope.isButtonClicked = true;
                if(!form.$valid){
                    return;
                }
                utilityService.showWaitLoader();
                var alertMethodName = 'showAlertMessage';
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_PASS;
                if($scope.$parent.userrole == 'candidate'){
                    var postData = {
                                "CID" :  $rootScope.loggedInUserId,
                                "newpassword" : $scope.updatePassControl.newpassword,
                                "userrole" : $scope.$parent.userrole
                            };
                }else if($scope.$parent.userrole == 'superadmin'){
                    var postData = {
                        "SID" :  $rootScope.loggedInUserId,
                        "newpassword" : $scope.updatePassControl.newpassword,
                        "userrole" : $scope.$parent.userrole 
                    };
                }else{
                    var postData = {
                        "CTID" :  $rootScope.loggedInUserId,
                        "newpassword" : $scope.updatePassControl.newpassword,
                        "userrole" : $scope.$parent.userrole 
                    };
                    alertMethodName = 'showCitizenAlertMessage';
                }
                
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    console.log('inside Update success',data);
                    utilityService.hideWaitLoader();
                    if(data.status == 'OK'){
                        utilityService[alertMethodName](data.message);
                        $uibModalInstance.close();
                    }else{
                        utilityService[alertMethodName](data.message);

                    }

                },function(err){
                    console.log('inside savePartyDetails error',err);
                    utilityService.hideWaitLoader();
                    utilityService[alertMethodName]("Something went wrong please try again");
                   
                });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

        

        $scope.cancelModal = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);






