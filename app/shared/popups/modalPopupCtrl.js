//;(function (module, undefined) {
    'use strict';

    angular.module('utilModule').controller('modalPopupCtrl',['$scope','$uibModalInstance','message',function($scope, $uibModalInstance,message) {
             $scope.message = message;

             $scope.ok = function () {
                $uibModalInstance.close();
            };

              $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };

    }]);


//})(socialApp.controllers);



