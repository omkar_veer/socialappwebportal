    'use strict';

    angular.module('utilModule').controller('OTPmodalPopupCtrl',['$scope','$uibModalInstance','$rootScope','utilityService',
        function($scope, $uibModalInstance,$rootScope,utilityService) {
             
        $scope.OTPControl = {};
        console.log("$scope.$parent.OTPForm",$scope.$parent.OTPForm);    

        $scope.submitCandidateOTP = function () {
           try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.VERIFY_OTP;
                    var postData = {
                                    "CID" :  $scope.$parent.OTPForm.CID,
                                    "phone" : $scope.$parent.OTPForm.phoneNumber,
                                    "otp" : $scope.OTPControl.OTP 
                                };
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside SubmitOTP success',data);
                        utilityService.hideWaitLoader();

                        if(typeof data == 'string'){
                                var splitedStr = data.split('{') //[1];
                                if(splitedStr.length != 0){
                                        //splitedStr[1];
                                        var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

                                        if(strRes.status == 'OK'){
                                            utilityService.showAlertMessage(strRes.message);
                                            $uibModalInstance.close();
                                            $rootScope.$broadcast('OTPVerified',{'CID':$scope.$parent.OTPForm.CID});

                                        }else{
                                            utilityService.showAlertMessage(strRes.message);

                                        }

                                }else{
                                    utilityService.showAlertMessage("Something went wrong please try again");
                                }
                            }else{
                                if(data.status == 'FAILED'){
                                    utilityService.showAlertMessage(data.message);  
                                }
                            }


                        /*if(data.status == 'OK'){
                            utilityService.showAlertMessage(data.message);
                            $uibModalInstance.close();
                            $rootScope.$broadcast('OTPVerified',{'CID':$scope.$parent.OTPForm.CID});
                        }else{
                            utilityService.showAlertMessage(data.message);

                        }*/

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

        $scope.submitCitizenOTP = function () {
           try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.VERIFY_CITIZEN_OTP;
                    var postData = {
                                    "CTID" :  $scope.$parent.OTPForm.CTID,
                                    "phone" : $scope.$parent.OTPForm.phoneNumber,
                                    "otp" : $scope.OTPControl.OTP 
                                };
                    if($scope.$parent.OTPForm.type == 'citizen'){
                        $scope.alertMethodName = 'showCitizenAlertMessage';
                    }else{
                        $scope.alertMethodName = 'showAlertMessage';
                    }           
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside SubmitOTP success',data);
                        utilityService.hideWaitLoader();

                        if(typeof data == 'string'){
                                var splitedStr = data.split('{') //[1];
                                if(splitedStr.length != 0){
                                        //splitedStr[1];
                                        var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

                                        if(strRes.status == 'OK'){
                                            utilityService[$scope.alertMethodName](strRes.message);
                                            $uibModalInstance.close();
                                            $rootScope.$broadcast('OTPVerified',{'CTID':$scope.$parent.OTPForm.CTID});

                                        }else{
                                            utilityService[$scope.alertMethodName](strRes.message);

                                        }

                                }else{
                                    utilityService[$scope.alertMethodName]("Something went wrong please try again");
                                }
                            }else{
                                if(data.status == 'FAILED'){
                                    utilityService[$scope.alertMethodName](data.message);  
                                }
                            }
                        


                        /*if(data.status == 'OK'){
                            utilityService[$scope.alertMethodName](data.message);
                            $uibModalInstance.close();
                            $rootScope.$broadcast('OTPVerified',{'CTID':$scope.$parent.OTPForm.CTID});
                        }else{
                            utilityService[$scope.alertMethodName](data.message);

                        }*/

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService[$scope.alertMethodName]("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

        $scope.SubmitOTP = function () {
           if($scope.$parent.OTPForm.type == 'candidate'){
                  $scope.submitCandidateOTP();
           }else if ($scope.$parent.OTPForm.type == 'citizen' || $scope.$parent.OTPForm.type == 'karyakarta'){
                 $scope.submitCitizenOTP();

           }
           
        };

        $scope.ResendOTP = function () {
            try{
                    if($scope.$parent.OTPForm.type == 'candidate'){
                        var postUrl = CONSTANTS.BASE_URL + CONSTANTS.RESEND_OTP;
                    }else if($scope.$parent.OTPForm.type == 'citizen' || $scope.$parent.OTPForm.type == 'karyakarta'){
                        var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_RESEND_OTP;
                    }
                    
                    var postData = {
                                    "phone" : $scope.$parent.OTPForm.phoneNumber
                                   };
                    utilityService.requestPOSTHttpService(postUrl,postData  ,function(data){
                        console.log('inside ResendOTP success',data);
                        
                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        
                    });
            

            }catch(e){
                    
            }
        };

        $scope.cancelOTPModal = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);






