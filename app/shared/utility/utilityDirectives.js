//;(function (module, undefined) {
    'use strict';

    angular.module('utilModule').directive('pwCheck', [function () {
        return {
          require: 'ngModel',
          link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            $(elem).add(firstPassword).on('keyup', function () {
              scope.$apply(function () {
                var v = elem.val()===$(firstPassword).val();
                ctrl.$setValidity('pwmatch', v);
              });
            });
          }
        }
    }]);


    angular.module('utilModule').directive('selectOnClick', ['$window', function ($window) {
      return {
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.on('click', function () {
                  if (!$window.getSelection().toString()) {
                      // Required for mobile Safari
                      this.setSelectionRange(0, this.value.length)
                  }
              });
          }
      };
    }]);

    angular.module('utilModule').filter('formatDate', function() {
      return function(x) {
          console.log("X at filer",new Date(x));
          return $.datepicker.formatDate( "yy-mm-dd", new Date( x ) );
      };
    });

    


    


//})(socialApp.directives);



