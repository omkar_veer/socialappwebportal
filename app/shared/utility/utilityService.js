//;(function (module, undefined) {
    'use strict';

    angular.module('utilModule').service('utilityService',['$rootScope','$http','$uibModal','usSpinnerService',
        function($rootScope,$http,$uibModal,usSpinnerService) {
        console.log("inside utilityService");

        return {
            authoriseUser :function(userSessionKey){
                    if(window.sessionStorage.getItem(userSessionKey)){
                       return true;
                    }else{
                       return false;
                    }
            },

            requestPOSTHttpService : function (requestUrl,data,success,error){
                    var config = {
                        //withCredentials: true,
                        url: requestUrl,
                        method: 'POST',
                        data:data
                    }

                    $http(config)
                        .success(function(data,status,headers,config){
                            console.log("In service success",data);
                            return success(data);
                        })
                        .error(function(data,status,headers,config){
                            console.log("In service error",data);
                            return error(data);
                    });
             },

             requestFilePOSTHttpService : function (requestUrl,data,success,error){
                    var config = {
                        //withCredentials: true,
                        url: requestUrl,
                        method: 'POST',
                        headers:{
                           'Content-Type': undefined,//'multipart/form-data'
                          //'Content-Type': false,//undefined,
                          //'Process-Data': false
                           //"multipart/form-data",
                          //'Content-Type':'application/x-www-form-urlencoded'
                          //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        transformRequest: function (data) {
                            var formData = new FormData();
                            //need to convert our json object to a string version of json otherwise
                            // the browser will do a 'toString()' on the object which will result 
                            // in the value '[Object object]' on the server.
                            /*formData.append("model", angular.toJson(data.model));
                            //now add all of the assigned files
                            for (var i = 0; i < data.files; i++) {
                                //add each file to the form data and iteratively name them
                                formData.append("file" + i, data.files[i]);
                            }*/

                            for(var i in data){
                               formData.append(i, data[i]);
                               
                            }

                            return formData;
                        },
                        //Create an object that contains the model and files which will be transformed
                        // in the above transformRequest method
                        //data: { model: $scope.model, files: $scope.files }
                        data:data
                    }

                    $http(config)
                        .success(function(data,status,headers,config){
                            console.log("In service success",data);
                            return success(data);
                        })
                        .error(function(data,status,headers,config){
                            console.log("In service error",data);
                            return error(data);
                    });
             },

             storeDataToLocalStorage:function(key,data){
                    window.sessionStorage.setItem(key,JSON.stringify(data));

             },

             getDataFromLocalStorage:function(key){
                    return JSON.parse(window.sessionStorage.getItem(key));
             },

             deleteSessionStorageItem:function(key){
                window.sessionStorage.removeItem(key);
             },

             showPopUp :function(popupConfig){
                  var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: popupConfig.url,//'myModalContent.html',
                              controller: popupConfig.ctrl,//'ModalInstanceCtrl',
                              //size: size,
                              backdrop:'static',
                              resolve: {
                                image: function () {
                                   return popupConfig.image;
                                },
                                params: function () {
                                   return popupConfig.params;
                                }

                                

                              }
                  });

                  return modalInstance;
             },

             showModalPopup:function(popupConfig){
                     var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: popupConfig.url,//'myModalContent.html',
                              controller: popupConfig.ctrl,//'ModalInstanceCtrl',
                              //size: size,
                              resolve: {
                                message: function () {
                                   return popupConfig.message;
                                }

                              }
                    });

                     return modalInstance;
             },

             showAlertMessage :function(message){
                    return this.showModalPopup({
                            'url':'./app/shared/popups/alertPopup.html',
                            'ctrl':'modalPopupCtrl',
                            'message': message
                    });
             },

             showBirthdayListPopup :function(message){
                    return this.showModalPopup({
                            'url':'./app/shared/popups/birthdayListPopup.html',
                            'ctrl':'modalPopupCtrl',
                            'message': message
                    });
             },

             showConfirmationAlertMessage:function(message){
                return this.showModalPopup({
                            'url':'./app/shared/popups/confirmAlertPopup.html',
                            'ctrl':'modalPopupCtrl',
                            'message': message
                    });
             },

             showInputAlertMessage:function(message){
                return this.showModalPopup({
                            'url':'./app/shared/popups/inputAlertPopup.html',
                            'ctrl':'modalPopupCtrl',
                            'message': message
                    });
             },

             showOTPAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: './app/shared/popups/OTPPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'OTPmodalPopupCtrl',
                              
                    });

                return modalInstance;
             },

             showNsForgotPassAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: './app/shared/popups/nsForgotPassPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'nsForgotPassPopupCtrl',
                              
                    });

                return modalInstance;
             },



             showCitizenForgotPassAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: '../app/shared/popups/nsForgotPassPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'nsForgotPassPopupCtrl',
                              
                    });

                return modalInstance;
             },

             showNsUpdatePassAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: './app/shared/popups/updatePassPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'UpdatePassPopupCtrl',
                              
                    });

                return modalInstance;
             },

             showCitizenUpdatePassAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: '../app/shared/popups/updatePassPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'UpdatePassPopupCtrl',
                              
                    });

                return modalInstance;
             },


             showCitizenOTPAlertMessage:function(scope){
                var modalInstance = $uibModal.open({
                              animation: true,
                              templateUrl: '../app/shared/popups/OTPPopup.html',
                              scope:scope,
                              backdrop:'static' ,
                              controller: 'OTPmodalPopupCtrl',
                              
                    });

                return modalInstance;
             },

             showCitizenAlertMessage :function(message){
                    return this.showModalPopup({
                            'url':'../app/shared/popups/alertPopup.html',
                            'ctrl':'modalPopupCtrl',
                            'message': message
                    });
             },

             

             showWaitLoader:function(){
                    $(".loader-overlay").show();
                    usSpinnerService.spin('spinner-1');

             },

             hideWaitLoader:function(overlayInstance){
                   $(".loader-overlay").hide();
                    usSpinnerService.stop('spinner-1');
             },

             getCastList:function(sucessCB,errorCB){
            
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_CAST_LIST;
                    this.requestPOSTHttpService(postUrl, {} ,function(data){
                        console.log('inside getCastList success',data);
                        
                        if(data.status == 'OK' && data.caste){
                            return sucessCB(data.caste);
                        }else{
                            console.log('inside getCastList error',data);
                             return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getCastList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                    return errorCB(e);
                }
             },

            getAllStates:function(sucessCB,errorCB){
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_STATES;
                    this.requestPOSTHttpService(postUrl, {} ,function(data){
                        console.log('inside getPoliticalPartyList success',data);
                        
                        if(data.status == 'OK' && data.states){
                           
                            return sucessCB(data.states);
                        }else{
                            console.log('inside getPoliticalPartyList error',data);
                            return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getPoliticalPartyList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                    return errorCB(e);
                }
                

            },

            getDistricts:function(state,sucessCB,errorCB){
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_DISTRICTS;
                    var postParam =  {"state":state};
                    this.requestPOSTHttpService(postUrl,postParam,function(data){
                        console.log('inside getPoliticalPartyList success',data);
                        
                        if(data.status == 'OK' && data.districts){
                            return sucessCB(data.districts);
                        }else{
                            console.log('inside getPoliticalPartyList error',data);
                            return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getPoliticalPartyList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                        console.log(e);
                        return errorCB(e);
                }
               

            },

           getTalukas:function(district,sucessCB,errorCB){
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_TALUKAS;
                    var postParam =  {"district": district};

                    this.requestPOSTHttpService(postUrl, postParam ,function(data){
                        console.log('inside getPoliticalPartyList success',data);
                        
                        if(data.status == 'OK' && data.talukas){
                            return sucessCB(data.talukas);
                        }else{
                            console.log('inside getPoliticalPartyList error',data);
                            return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getPoliticalPartyList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                        return errorCB(e);
                }
            
            },


            getPincodes:function(taluka,sucessCB,errorCB){
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_PINCODES;
                    var postParam =  {"taluka":taluka};
                    this.requestPOSTHttpService(postUrl, postParam ,function(data){
                        console.log('inside getPoliticalPartyList success',data);
                        
                        if(data.status == 'OK' && data.pincodes){
                            return sucessCB(data.pincodes);
                        }else{
                            console.log('inside getPoliticalPartyList error',data);
                            return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getPoliticalPartyList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                    return errorCB(e);
                } 
        

            },

            getReligionList:function(sucessCB,errorCB){
                try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_RELIGION;
                    this.requestPOSTHttpService(postUrl, {} ,function(data){
                        console.log('inside getReligionList success',data);
                        
                        if(data.status == 'OK' && data.religion){
                           
                            return sucessCB(data.religion);
                        }else{
                            console.log('inside getReligionList error',data);
                            return errorCB(data);
                        }
                        
                    },function(err){
                        console.log('inside getReligionList error',err);
                        return errorCB(err);
                        
                    });
                }catch(e){
                    return errorCB(e);
                }
            },

            getComplaints:function(postData,sucessCB,errorCB){
                try{
                  var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_COMPLAINTS;
                  this.requestPOSTHttpService(postUrl, postData ,function(data){
                    console.log('inside getComplaints success',data);
                    return sucessCB(data);
                    
                  },function(err){
                        console.log('inside getComplaintCategories error',err);
                        return errorCB(err);
                        
                  });
                }catch(e){
                     return errorCB(e);
                }
            }
        }

    }]);


//})(socialApp.services);



