'use strict';

angular.module('utilModule').controller('mainCtrl',['$scope','$rootScope','utilityService','$state', 
    function($scope, $rootScope,utilityService,$state) {
        console.log("inside mainCtrl");
      
        if(utilityService.getDataFromLocalStorage('CTID')){ //LoggedIn as Citizen
        		$rootScope.loggedInUserId = utilityService.getDataFromLocalStorage('CTID');
        		
        }

        $scope.getLoggedInUserDetails = function (){
        	console.log("inside getLoggedInUserDetails");
        	var type = '';
            $('.header-user-name').removeClass('hide');
        	if(utilityService.getDataFromLocalStorage('CTID') && (utilityService.getDataFromLocalStorage('CTID') != null)){ //Citizen
        		type = 'citizen';
        	}
        	var postData = {'id':$rootScope.loggedInUserId, 'type':type};
        	var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_LOGGEDIN_USER_DETAILS;
        	
        	utilityService.requestPOSTHttpService(postUrl,postData ,function(data){
        		  if(data.status == 'OK'){
        		  		$rootScope.loggedInUserDetails = data.user;
                        if(!$rootScope.loggedInUserDetails.image){
                            $rootScope.loggedInUserDetails.image = '../assets/img/user.png';
                        }
        		  }
        	},function(err){
        		   console.log("Error Occured");
        	});
        }

        $scope.openUpdatePassPopUp = function(){
            $scope.userrole = 'citizen';
            utilityService.showCitizenUpdatePassAlertMessage($scope);
            
        };

    

    }
]);


