'use strict';

angular.module('utilModule').controller('utilityCtrl',['$scope','$rootScope','utilityService','$state', 
    function($scope, $rootScope,utilityService,$state) {
        console.log("inside utilityCtrl");
      
        if(utilityService.getDataFromLocalStorage('SSID')){ //LoggedIn as SuperAdmin
        		$rootScope.loggedInUserId = utilityService.getDataFromLocalStorage('SSID');
        		
        }else if(utilityService.getDataFromLocalStorage('CID')){ //LoggedIn as Admin
        		$rootScope.loggedInUserId = utilityService.getDataFromLocalStorage('CID');
                
        } 


        $scope.getLoggedInUserDetails = function (){
        	console.log("inside getLoggedInUserDetails");
        	var type = '';
        	$('.header-user-name').removeClass('hide');
        	if(utilityService.getDataFromLocalStorage('SSID') && (utilityService.getDataFromLocalStorage('SSID') != null)){ //LoggedInAsSuperAdmin
        		type = 'superadmin';
        	}else if(utilityService.getDataFromLocalStorage('CID') && (utilityService.getDataFromLocalStorage('CID') != null)){//LoggedInAsAdmin
        		type = 'candidate';
        	}
        	var postData = {'id':$rootScope.loggedInUserId, 'type':type};
        	var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_LOGGEDIN_USER_DETAILS;
        	
        	utilityService.requestPOSTHttpService(postUrl,postData ,function(data){
        		  if(data.status == 'OK'){
        		  		$rootScope.loggedInUserDetails = data.user;
                        if(!$rootScope.loggedInUserDetails.image){
                            $rootScope.loggedInUserDetails.image = './assets/img/user.png';
                        }
        		  }
        	},function(err){
        		   console.log("Error Occured");
        	});
        }

         $scope.openUpdatePassPopUp = function(){
            if(utilityService.getDataFromLocalStorage('SSID') && (utilityService.getDataFromLocalStorage('SSID') != null)){ //LoggedInAsSuperAdmin
                $scope.userrole = 'superadmin';
            }else if(utilityService.getDataFromLocalStorage('CID') && (utilityService.getDataFromLocalStorage('CID') != null)){//LoggedInAsAdmin
                $scope.userrole = 'candidate';
            }
            
            utilityService.showNsUpdatePassAlertMessage($scope);
            
        };

    

    }
]);


