var CONSTANTS = {
	'STATE_TRANS_OPTIONS':{ reload: true, inherit: true, notify: true },
        //'APP_MODE':'PROD'
        'APP_MODE':'DEV',
        'STATE_TRANS_OPTIONS':{ reload: true, inherit: true, notify: true },

        'HARDCODED_VALUES':{
                'PROFESSION_LIST':['Teacher','IT/Telecom Professional','Engineer',
                                   'Doctor','Lawyer','Electrician','Fabricator',
                                   'Plumber','Carpenter','Govt. Servant',
                                   'Pvt. Service','Self-employed/Business owner'
                ],
                'EDUCATION_LIST':['Illiterate ','Up to SSC','Up to HSC','ITI','Diploma',
                                'Graduate – B.A.','Graduate – B.Com.','Graduate – B.Sc.',
                                'Graduate – B.Pharm.','Graduate – B.Sc.Agri.','Graduate – BCA/BBA/BCS',
                                'Post Graduate – M.A.','Post Graduate – M.Com.','Post Graduate – M.Sc.',
                                'Post Graduate – M.Pharm.','Post Graduate – M.Sc.Agri.','Graduate – MCA/MBA/MCS',
                                'B.E./B.Tech.','M.E./M.Tech.','MBBS','BHMS','BAMS','PGDBM','PhD'
                ],
                'BLOODGRP_LIST':['O+','O-','A-','A+','B+','B-','AB+','AB-'],
                'EMERGENCY_SERVICES':['Police','Ambulance','Hospital','Fire Brigade','Other'],
                'CORPORATION_SERVICES':['Ration Card','Adhar Card','Death Certificate','Marriage Certificate',
                                        'Water Supplier','Voter Registration','EBC SUBSIDY' ,'Other'
                                       ],
                'COMMERCIAL_SERVICES':['Plumbing','Electrician','Carpenter','Lawyer','Fabricator',
                                        'LPG','2-Wheeler Mechanic','4-Wheeler Mechanic','Other'

                                      ]

        },




        //'BASE_URL':'http://anithedesigner.in/sapp/backoffice/webservice/',
        'BASE_URL':'http://nagarisuvidha.com/sapp/backoffice/webservice/',
        'SUPER_ADMIN_REGI_URL':'suregister.php',
        'SUPER_ADMIN_LOGIN_URL':'sulogin.php',
        'CREATE_CANDIDATE_URL':'candidateregister.php',
        'LOCATION_MASTER_URL':'locationsmaster.php',
        'GET_ALL_CANDIDATE':'getallcandidate.php',
        'GET_WARD_DETAILS':'getward.php',
        'GET_PARTY_LIST':'getallparties.php',
        'ADMIN_LOGIN_URL':'sulogindd.php',
        'CANDIDATE_LOGIN_URL':'candidatelogin.php',
        'GET_ALL_STATES':'getallstates.php',
        'GET_DISTRICTS':'getalldistricts.php',
        'GET_TALUKAS':'getalltalukas.php',
        'GET_PINCODES':'getallpincodes.php',
        'GET_LOKSABHAS':'getloksabha.php',
        'GET_VIDHASABHAS':'getvidhansabha.php',
        'SAVE_PARTY_DETAILS_URL': 'insertorupdatepartyprofile.php',//'partyprofiletab.php',
        'SAVE_JAGIRNAMA_DETAILS_URL':'insertorupdatejahirnama.php' ,//'jahirnamatab.php',
        'SAVE_WORK_DETAILS_URL':'worktab.php',
        'GET_CANDIDATE_DATA':'getcandidatedata.php',
        'UPDATE_PRPOFILE_TAB':'updateprofiletab.php',
        'UPDATE_WORK_TAB':'updateworktab.php',
        'DELETE_CANDIDATE':'deletecandidate.php',
        'VERIFY_OTP':'verifycandidate.php',
        'RESEND_OTP':'resendotpcandidate.php',
        'SAVE_JAHIRNAMA_WORKS':'insertjahirnamaorwork.php',
        'DELETE_JAHIRNAMA_WORK':'deletejahirnamawork.php',
        'GET_ALL_JAHIRNAMA_WORK':'getalljahirnamaorwork.php',
        'GET_CAST_LIST':'getcastemaster.php',
        'CITIZEN_REGISTRATION':'citizenregister.php',
        'CITIZEN_RESEND_OTP':'resendotp.php',
        'VERIFY_CITIZEN_OTP':'verifycitizen.php',
        'CITIZEN_LOGIN':'citizenlogin.php',
        'CITIZEN_LOGOUT':'citizenlogout.php',
        'BIRTHDAY_LIST':'bdaylist.php',
        'GET_ALL_RELIGION':'getreligionmaster.php',
        'GET_LOGGEDIN_USER_DETAILS':'getloggedinuserdetails.php',
        'ADD_SERVICE_COMPLAINT_URL':'addservicecomplaint.php',
        'GET_SERVICES':'getservicetype.php',
        'ADD_NEW_SERVICE':'addservice.php',
        'UPDATE_SERVICE':'updateservice.php',
        'ADD_COMPLAINT_URL':'addcomplaint.php',
        'GET_COMPLAINT_CATEGORY':'getcomplaintcategorymaster.php',
        'GET_ALL_COMPLAINTS':'getallcomplaints.php',
        'RESOLVE_COMPLAINTS':'resolvedcomplaint.php',
        'GET_COMPLAINT_DETAILS':'getsinglecomplaint.php',
        'ADD_NEW_HELPING_HAND':'addhhdata.php',
        'GET_HELPING_HAND_LIST':'gethhdata.php',
        'UPDATE_HELPING_HAND':'updatehhdata.php',
        'DELETE_HELPING_HAND':'deletehhdata.php',
        'DELETE_SERVICES':'deleteservicecategory.php',
        'SEND_ANNOUNCEMENT':'announcement.php',
        'SEND_EVENT_OF_WEEK':'eventoftheweek.php',
        'IMPORT_CSV':'importcsv.php',
        'ADD_MATDAR_JAGRUTI':'addmjdata.php',
        'UPDATE_MATDAR_JAGRUTI':'updatemjdata.php',
        'DELETE_MATDAR_JAGRUTI':'deletemjdata.php',
        'GET_MATDAR_JAGRUTI':'getmjdata.php',
        'SOCIAL_URL':'socialurl.php',
        'SOCIAL_URL_GET':'socialurl_mobile.php',
        'UPDATE_PASS':'updaterolepassword.php',
        'FORGOT_PASS':'forgotpassword.php',
        'GET_DASHBOARD_DATA':'getdashboardstat.php',
        'BULK_SMS_SERVICE':'bulksmsaction.php',
        'COMPLAINT_STATISTICS':'complaintstatistics.php'

};


