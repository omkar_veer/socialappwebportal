
    'use strict';

    angular.module('karyakartaApp',
                          [
                           'utilModule'
                          ])

     .config(['$stateProvider', '$urlRouterProvider','$translateProvider', '$provide','$httpProvider',
      function($stateProvider, $urlRouterProvider,$translateProvider, $provide,$httpProvider) {
            
            $httpProvider.defaults.timeout = 10000;
            $httpProvider.defaults.headers.common = {};
            $httpProvider.defaults.headers.post = {};
            $httpProvider.defaults.headers.put = {};
            $httpProvider.defaults.headers.patch = {};
           
            $stateProvider

              .state('login', {
                  url: "/login?cid",
                  controller: 'karyakartaLoginCtrl',
                  templateUrl: "../app/components/karyakartaManager/login/login.html",
                  cache:false
              })

              .state('add-citizen', {
                  url: "/add-citizen?cid",
                  controller: 'addCitizenCtrl',
                  templateUrl: "../app/components/karyakartaManager/add-citizen/addcitizen.html",
                  cache:false,
                  resolve: {
                      isAuthorised: function(){
                        return (window.sessionStorage.getItem('CTID')) ? true :false;
                      }
                  }
              })

              
              .state('error', {
                  url: "/error",
                  templateUrl: "../app/shared/error/error.html",
              })

              
              // if none of the above states are matched, use this as the fallback
              $urlRouterProvider.otherwise('/error');


               $translateProvider.translations('en',en);
               $translateProvider.translations('mr',mr);

               $translateProvider.preferredLanguage("mr");
               $translateProvider.fallbackLanguage("en");

               // use the HTML5 History API
               //$locationProvider.html5Mode(true);

               // Enable escaping of HTML
              //$translateProvider.useSanitizeValueStrategy('sanitize');


              


        }]).

        run(['$rootScope',function($rootScope){

        }]);
