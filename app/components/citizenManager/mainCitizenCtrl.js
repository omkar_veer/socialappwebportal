
    'use strict';

    angular.module('citizenApp').controller('mainCitizenCtrl',['$scope','$rootScope','utilityService','$state','$stateParams', 
    	function($scope,$rootScope,utilityService,$state,$stateParams) {
         console.log("inside mainCitizenCtrl");
          window.sessionStorage.clear();
          $('.header-user-name').addClass('hide');
        $scope.citizenFormControl = {};
        $scope.CitizenOTPControl = {};
        $scope.citizenLoginControl = {};
        $scope.citizenFormControl.profile = {
        	'isVoter':1,
        	'role':'citizen',
        	'ischairman':0

        };

        addGlobalEventHandler();
        
        

        $scope.professionList = CONSTANTS.HARDCODED_VALUES.PROFESSION_LIST;
        $scope.educationList = CONSTANTS.HARDCODED_VALUES.EDUCATION_LIST;
        $scope.bloodgrpList = CONSTANTS.HARDCODED_VALUES.BLOODGRP_LIST;

        $rootScope.parentCandidateId = $stateParams.cid ; //utilityService.getDataFromLocalStorage('CID');   // storeDataToLocalStorage('CID'

        $scope.OTPForm = {};

        $scope.enableLoginSection = function(){
            $scope.isButtonClicked = false;
            $scope.showLogin = true;
            $scope.showRegistration = false;
            $scope.showOTPVerification = false;
            addGlobalEventHandler();
        }

        $scope.enableRegisterSection = function(){
            $scope.isButtonClicked = false;
            $scope.showLogin = false;
            $scope.showRegistration = true;
            $scope.showOTPVerification = false;
            addGlobalEventHandler();
        }

        $scope.enableOTPVerificationSection = function(){
            $scope.isButtonClicked = false;
            $scope.showLogin = false;
            $scope.showRegistration = false;
            $scope.showOTPVerification = true;
            addGlobalEventHandler();
        }

        $scope.openForgotPassPopUp = function(){
            $scope.userrole = 'citizen'; 
            utilityService.showCitizenForgotPassAlertMessage($scope);
            
        };


        //Login Module Starts
        $scope.citizenLogin = function(citizenloginform){
            
            try{
                    console.log("inside citizenLoginControl",$scope.citizenLoginControl);
                    $scope.isButtonClicked = true;

                    if(!citizenloginform.$valid){
                        $("[name='" + citizenloginform.$name + "']").find('.ng-invalid:visible:first').focus();
                        return;
                    }
                    var postUrl;
                    postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_LOGIN;    
                    utilityService.showWaitLoader();
                    
                    var postData = {
                                    "method":"login",
                                    "email":$scope.citizenLoginControl.username,
                                    "password":$scope.citizenLoginControl.password,
                                    "role":"citizen"
                                };
                    
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside login success',data.error);
                        utilityService.hideWaitLoader();
                         $scope.isButtonClicked = false;
                        if(data.status == 'OK'){
                                if(data.CTID){
                                    utilityService.storeDataToLocalStorage('CTID', data.CTID);
                                    utilityService.storeDataToLocalStorage('CID', data.CID);
                                    $state.transitionTo('raisecomplaints',{}, CONSTANTS.STATE_TRANS_OPTIONS);
                                }
                                
                                utilityService.hideWaitLoader();

                        }else if(data.status == '0'){
                                $scope.CitizenOTPControl.phone = data.phone;
                                $scope.CitizenOTPControl.CTID = data.CTID;
                                //$scope.CitizenOTPControl.firstname = $scope.citizenFormControl.profile.firstname;
                                $scope.enableOTPVerificationSection();
                        }else{
                            if(data.message){
                                $scope.loginErrMsg = data.message;
                                utilityService.hideWaitLoader();
                            }
                            
                        }

                        
                                                
                    },function(err){
                        console.log('inside login error',err);
                        alert("Something went wrong please try again");
                        utilityService.hideWaitLoader();
                    });
                }catch(e){
                    alert("Exeption occured, please try again");
                    utilityService.hideWaitLoader();
                }
        }
        //Login Ends


        //OTP Verification Module Starts
        $scope.ResendOTP = function () {
            try{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_RESEND_OTP;
                    var postData = {
                                    "phone" : $scope.CitizenOTPControl.phone
                                   };
                    utilityService.requestPOSTHttpService(postUrl,postData  ,function(data){
                        console.log('inside ResendOTP success',data);
                        
                    },function(err){
                        console.log('inside ResendOTP error',err);
                        
                    });
            

            }catch(e){
                    
            }
        };


        $scope.submitCitizenOTP = function () {
           try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.VERIFY_CITIZEN_OTP;
                    var postData = {
                                    "CTID" :  $scope.CitizenOTPControl.CTID,
                                    "phone" : $scope.CitizenOTPControl.phone,
                                    "otp" : $scope.CitizenOTPControl.OTP
                                };
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside SubmitOTP success',data);
                        utilityService.hideWaitLoader();

                        if(typeof data == 'string'){
                            var splitedStr = data.split('{') //[1];
                            if(splitedStr.length != 0){
                                    //splitedStr[1];
                                    var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

                                    if(strRes.status == 'OK'){
                                        $state.transitionTo('home',{}, CONSTANTS.STATE_TRANS_OPTIONS);

                                    }else{
                                        utilityService.showAlertMessage(strRes.message);

                                    }

                            }else{
                                utilityService.showAlertMessage("Something went wrong please try again");
                            }
                        }else{
                            if(data.status == 'FAILED'){
                                utilityService.showAlertMessage(data.message);  
                            }
                        }

                        /*if(data.status == 'OK'){
                            $state.transitionTo('home',{}, CONSTANTS.STATE_TRANS_OPTIONS);
                        }else{
                            utilityService.showCitizenAlertMessage(data.message);

                        }*/

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showCitizenAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

        //OTP Verification Module End


        var date = new Date()
        date.setMonth(date.getMonth() - 216);
        $scope.maxDateLimit = date;
        $scope.minDateLimit = new Date('1900/1/1');
        $scope.dateFormat = 'yyyy-MM-dd';
        //$scope.dateFormat = 'dd/MM/yyyy';

        $scope.getCastList = function(){
            utilityService.getCastList(function(data){
                $scope.casteList = data;
            },function(){

            });
        }

        $scope.getAllStates = function(){
            utilityService.getAllStates(function(data){
                $scope.states = data;
            },function(){

            });

        }

        $scope.getDistricts = function(state){
            utilityService.getDistricts( state , function(data){
                $scope.districts = data;
            },function(){

            });


        }

        $scope.getTalukas = function(district){
            utilityService.getTalukas(district,function(data){
                $scope.talukas = data;
            },function(){

            });

        }


        $scope.getPincodes = function(taluka){
            utilityService.getPincodes(taluka,function(data){
                $scope.pincodes = data;
            },function(){

            });

        }

        $scope.getReligionList=  function(){
            utilityService.getReligionList(function(data){
                $scope.religionList = data;
            },function(){

            });
        }

        $scope.$on('OTPVerified', function(event,data){
           
            $scope.citizenFormControl = {};
            $scope.citizenFormControl.profile = {
                'isVoter':1,
                'role':'citizen',
                'ischairman':0

            };
             $scope.addcitizenform.$setPristine();
             $scope.addcitizenform.$setUntouched();
             $scope.isButtonClicked = false;

        });

        $scope.createCitizenProfile = function(addcitizenform ){
            try{
                    console.log("inside candidate profile save",$scope.citizenFormControl);
                    $scope.isButtonClicked = true;
                   
                    if(!addcitizenform .$valid){
                        $("[name='" + addcitizenform.$name + "']").find('.ng-invalid:visible:first').focus();
                        return;
                    }
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_REGISTRATION;
                    
                    $scope.citizenFormControl.profile['CID'] = $stateParams.cid;//utilityService.getDataFromLocalStorage('CID');
                    $scope.citizenFormControl.profile['registeredFrom'] = 'Portal';
                    console.log('Before save',$scope.citizenFormControl.profile);
                    //return;
                    utilityService.requestPOSTHttpService(postUrl, $scope.citizenFormControl.profile ,function(data){
                        console.log('inside create candidate success',data);
                        //$scope.locationMasterData = data;
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                            if(typeof data == 'string'){
                            	var splitedStr = data.split('{') //[1];
	                            if(splitedStr.length != 0){
	                                    //splitedStr[1];
	                                    var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

	                                    if(strRes.status == 'OK'){

	                                        if(strRes.CTID){
	                                            utilityService.storeDataToLocalStorage('CTID', strRes.CTID);
                                                utilityService.storeDataToLocalStorage('CID', strRes.CID);

                                                $scope.CitizenOTPControl.phone = $scope.citizenFormControl.profile.phone;
	                                            $scope.CitizenOTPControl.CTID = strRes.CTID;
                                                $scope.CitizenOTPControl.firstname = $scope.citizenFormControl.profile.firstname;
	                                            /*$scope.OTPForm.type = 'citizen';
	                                            utilityService.showCitizenOTPAlertMessage($scope);*/

                                                $scope.enableOTPVerificationSection();
	                                            
	                                        }

	                                    }else{
	                                        utilityService.showCitizenAlertMessage(strRes.message);

	                                    }

	                            }else{
	                                utilityService.showCitizenAlertMessage("Something went wrong please try again");
	                            }
                            }else{
                            	if(data.status == 'FAILED'){
                            		/*if(data.otp == 'pending'){

                            		}else{*/
                            			utilityService.showCitizenAlertMessage(data.message);	
                            		//}
                            		

                            	}
                            }
                            
                        
                        
                    },function(err){
                        console.log('inside create manager error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showCitizenAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }

        



        

       

    }]);


