
    'use strict';

    angular.module('citizenApp').controller('complaintManagerCtrl',['$scope','$rootScope','utilityService','$state', 
        function($scope,$rootScope,utilityService,$state) {
        console.log("inside complaintManagerCtrl");
      
        /*if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }*/
        var CM = $scope;
        CM.raiseComplaintsFormControl = {};
        $rootScope.parentCandidateId = utilityService.getDataFromLocalStorage('CID');


        CM.raiseCompalint = function(raisecomplaintsform){
        	
            try{
        		CM.isButtonClicked = true;
                if(!raisecomplaintsform.$valid){
                    $("[name='" + raisecomplaintsform.$name + "']").find('.ng-invalid:visible:first').focus();
                            return;
                }
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.ADD_COMPLAINT_URL;
                CM.raiseComplaintsFormControl['CID'] = utilityService.getDataFromLocalStorage('CID');
                CM.raiseComplaintsFormControl['CTID'] = utilityService.getDataFromLocalStorage('CTID');

                utilityService.requestPOSTHttpService(postUrl, CM.raiseComplaintsFormControl ,function(data){
                	 console.log("inside raiseCompalint success",data);
                	 utilityService.hideWaitLoader();
                     if(data.status == 'OK'){
                	 	 utilityService.showCitizenAlertMessage(data.message);
                	 	 $state.transitionTo('mycomplaints',{}, CONSTANTS.STATE_TRANS_OPTIONS);
                	 }else{
                        utilityService.showCitizenAlertMessage(data.message);
                     }
                },function(error){
                    utilityService.hideWaitLoader();
                	utilityService.showCitizenAlertMessage("Something went wrong please try again");	
                });
        	}catch(ex){
                utilityService.hideWaitLoader();
        		console.log(ex);
        		utilityService.showCitizenAlertMessage("Something went wrong please try again");
        	}

        }

        CM.getComplaintCategories = function(){
            
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_COMPLAINT_CATEGORY;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getComplaintCategories success',data);
                    
                    if(data.status == 'OK' && data.complaintcategory){
                        $scope.complaintcategoryList = data.complaintcategory;
                    }else{
                        console.log('inside getCastList error',data);
                    }
                    
                },function(err){
                    console.log('inside getComplaintCategories error',err);
                    
                });
            }catch(e){
                console.log('inside getComplaintCategories error',e);
            }
        }


    }]);


