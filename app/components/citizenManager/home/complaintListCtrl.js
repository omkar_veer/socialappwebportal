
    'use strict';

    angular.module('citizenApp').controller('complaintListCtrl',['$scope','$rootScope','utilityService','$state', 
        function($scope,$rootScope,utilityService,$state) {
        console.log("inside complaintListCtrl");
      
        /*if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }*/

        $rootScope.parentCandidateId = utilityService.getDataFromLocalStorage('CID');

        var CL = $scope;
        CL.itemsPerPage = 10;
        CL.currentPage = 1;

        CL.getComplaints = function(){
            utilityService.showWaitLoader();
            try{
                var postData = {'CTID':utilityService.getDataFromLocalStorage('CTID'),'complaintsfor':'citizen'};
                utilityService.getComplaints(postData,function(data){
                	utilityService.hideWaitLoader();
                	if(data.status == 'OK' && data.complaintlist){
                        $scope.myComplaintList = data.complaintlist;
                    }else{
                        console.log('inside getCastList error',data);
                        utilityService.showCitizenAlertMessage(data.message);
                    }
                },function(err){
                	utilityService.hideWaitLoader();
                	utilityService.showCitizenAlertMessage("Something went wrong please try again");
                });
   
            }catch(e){
            	utilityService.hideWaitLoader();
                console.log('inside getComplaintCategories error',e);
                utilityService.showCitizenAlertMessage("Something went wrong please try again");
            }
        }


    }]);


