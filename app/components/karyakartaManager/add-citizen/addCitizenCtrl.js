
    'use strict';

    angular.module('karyakartaApp').controller('addCitizenCtrl',['$scope','$rootScope','utilityService','$state', 
    	function($scope,$rootScope,utilityService,$state) {
         console.log("inside addCitizenCtrl");

        $scope.citizenFormControl = {};
        $scope.citizenFormControl.profile = {
        	'isVoter':1,
        	'role':'citizen',
        	'ischairman':0

        };
        addGlobalEventHandler();
        $scope.professionList = CONSTANTS.HARDCODED_VALUES.PROFESSION_LIST;
        $scope.educationList = CONSTANTS.HARDCODED_VALUES.EDUCATION_LIST;
        $scope.bloodgrpList = CONSTANTS.HARDCODED_VALUES.BLOODGRP_LIST;

        $rootScope.parentCandidateId = utilityService.getDataFromLocalStorage('CID');   // storeDataToLocalStorage('CID'

        $scope.OTPForm = {};

        var date = new Date()
        date.setMonth(date.getMonth() - 216);
        $scope.maxDateLimit = date;
        $scope.minDateLimit = new Date('1900/1/1');
        $scope.dateFormat = 'yyyy-MM-dd';
        //$scope.dateFormat = 'dd/MM/yyyy';

        $scope.getCastList = function(){
            utilityService.getCastList(function(data){
                $scope.casteList = data;
            },function(){

            });
        }

        $scope.getAllStates = function(){
            utilityService.getAllStates(function(data){
                $scope.states = data;
            },function(){

            });

        }

        $scope.getDistricts = function(state){
            utilityService.getDistricts( state , function(data){
                $scope.districts = data;
            },function(){

            });


        }

        $scope.getTalukas = function(district){
            utilityService.getTalukas(district,function(data){
                $scope.talukas = data;
            },function(){

            });

        }


        $scope.getPincodes = function(taluka){
            utilityService.getPincodes(taluka,function(data){
                $scope.pincodes = data;
            },function(){

            });

        }

        $scope.getReligionList=  function(){
            utilityService.getReligionList(function(data){
                $scope.religionList = data;
            },function(){

            });
        }

        $scope.$on('OTPVerified', function(event,data){
           
            $scope.citizenFormControl = {};
            $scope.citizenFormControl.profile = {
                'isVoter':1,
                'role':'citizen',
                'ischairman':0

            };
             $scope.addcitizenform.$setPristine();
             $scope.addcitizenform.$setUntouched();
             $scope.isButtonClicked = false;

        });

        $scope.createCitizenProfile = function(addcitizenform ){
            try{
                    console.log("inside candidate profile save",$scope.citizenFormControl);
                    $scope.isButtonClicked = true;
                   
                    if(!addcitizenform .$valid){
                        $("[name='" + addcitizenform.$name + "']").find('.ng-invalid:visible:first').focus();
                            return;
                    }
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_REGISTRATION;
                    
                    $scope.citizenFormControl.profile['CID'] = utilityService.getDataFromLocalStorage('CID');
                    $scope.citizenFormControl.profile['registeredFrom'] = 'Portal';
                    
                    console.log('Before save',$scope.citizenFormControl.profile);
                    //return;
                    utilityService.requestPOSTHttpService(postUrl, $scope.citizenFormControl.profile ,function(data){
                        console.log('inside create candidate success',data);
                        //$scope.locationMasterData = data;
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                            if(typeof data == 'string'){
                            	var splitedStr = data.split('{') //[1];
	                            if(splitedStr.length != 0){
	                                    //splitedStr[1];
	                                    var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

	                                    if(strRes.status == 'OK'){
	                                        if(strRes.CTID){
	                                            $scope.OTPForm.phoneNumber = $scope.citizenFormControl.profile.phone;
	                                            $scope.OTPForm.CTID = strRes.CTID;
	                                            $scope.OTPForm.type = 'citizen';
	                                            utilityService.showCitizenOTPAlertMessage($scope);
	                                            
	                                        }

	                                    }else{
	                                        utilityService.showCitizenAlertMessage(strRes.message);

	                                    }

	                            }else{
	                                utilityService.showCitizenAlertMessage("Something went wrong please try again");
	                            }
                            }else{
                            	if(data.status == 'FAILED'){
                            		/*if(data.otp == 'pending'){

                            		}else{*/
                            			utilityService.showCitizenAlertMessage(data.message);	
                            		//}
                            		

                            	}
                            }
                            
                        
                        
                    },function(err){
                        console.log('inside create manager error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showCitizenAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }

        



        

       

    }]);


