//;(function (module, undefined) {
    'use strict';

    angular.module('karyakartaApp').controller('karyakartaLoginCtrl',['$scope','$rootScope','utilityService','$state', function($scope,$rootScope,utilityService,$state) {
         console.log("inside karyakartaLoginCtrl");
         window.sessionStorage.clear();
         addGlobalEventHandler();
         $('.header-user-name').addClass('hide');

        $scope.karyakartaLogin = function(karyakartaloginform){
        		try{
        			console.log("inside karyakartaloginform",$scope.karyakartaloginControl);
	        		$scope.isButtonClicked = true;

	        		if(!karyakartaloginform.$valid){
                        $("[name='" + karyakartaloginform.$name + "']").find('.ng-invalid:visible:first').focus();
	        				return;
	        		}
                    var postUrl;
                    postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_LOGIN;    
                    utilityService.showWaitLoader();
	        		
	        		var postData = {"method":"login",
                                    "email":$scope.karyakartaloginControl.username,
                                    "password":$scope.karyakartaloginControl.password,
                                    "role":"manager"
                                };
	        		
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
	        			console.log('inside login success',data.error);
                        utilityService.hideWaitLoader();
                        if(data.status == 'OK'){
                                if(data.CTID){
                                    utilityService.storeDataToLocalStorage('CTID', data.CTID);
                                    utilityService.storeDataToLocalStorage('CID', data.CID);
                                    $state.transitionTo('add-citizen',{'cid':data.CID}, CONSTANTS.STATE_TRANS_OPTIONS);
                                }
                                
                                utilityService.hideWaitLoader();

                        }else{
                            if(data.message){
                                $scope.loginErrMsg = data.message;
                                utilityService.hideWaitLoader();
                            }
                            
                        }

                        
	        				        			
	        		},function(err){
	        			console.log('inside login error',err);
	        			alert("Something went wrong please try again");
                        utilityService.hideWaitLoader();
	        		});
        		}catch(e){
        			alert("Exeption occured, please try again");
                    utilityService.hideWaitLoader();
        		}
        		
        }

        $scope.openForgotPassPopUp = function(){
            $scope.userrole = 'citizen'; 
            utilityService.showCitizenForgotPassAlertMessage($scope);
            
        };

       

    }]);


//})(socialApp.controllers);