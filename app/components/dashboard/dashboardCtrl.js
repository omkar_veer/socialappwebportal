//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('dashboardCtrl',['$scope','$rootScope','utilityService','$state','isAuthorised', 
        function($scope, $rootScope,utilityService,$state,isAuthorised) {
        console.log("inside dashboardCtrl");
      
         if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }


        $scope.getDahsboardStatistics = function(userroleType){
               console.log("Inside getDahsboardStatistics",userroleType);
               try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_DASHBOARD_DATA;
                var postData = {};
                if(userroleType == 'candidate'){
                    postData = {
                        'CID':$rootScope.loggedInUserId,
                        'userrole':userroleType
                    };
                }else{
                    postData = {
                        'SID':$rootScope.loggedInUserId,
                        'userrole':userroleType
                    };
                }
                    
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    console.log('inside getDahsboardStatistics success',data);
                    if(data.status == 'OK'){
                        if(userroleType == 'candidate'){
                            $scope.dashboardData = data.candidatestat;
                        }else{
                            $scope.dashboardData = data.superadminstat;
                        }

                    }else{
                        utilityService.showAlertMessage("Something went wrong please try again");
                    }
                    
                    utilityService.hideWaitLoader();
                },function(err){
                    console.log('inside getDahsboardStatistics error',err);
                    //alert("Something went wrong please try again");
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });
            }catch(e){
                console.log('inside getCandidateList error',e);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }  
        }

        $scope.initializeAdminMode = function(){
            $scope.getShareLink = function(candidate){
                var citizenRegilink = window.location.origin + window.location.pathname +'citizen/#/main?cid='+ utilityService.getDataFromLocalStorage('CID');
                var managerLoginlink = window.location.origin + window.location.pathname +'karyakarta/#/login?cid='+ utilityService.getDataFromLocalStorage('CID');
                var modalInstance = utilityService.showInputAlertMessage({
                        'citizenRegi':citizenRegilink,
                        'managerLogin':managerLoginlink
                });
                   

                modalInstance.result.then(function () {
                  
                }, function () {
                    console.log("cancled");  
                });
            }
            $scope.getDahsboardStatistics($scope.userrole);

        }

        $scope.initializeSuperAdminMode = function(){
            $scope.getDahsboardStatistics($scope.userrole);
        }

        
       
        if(utilityService.getDataFromLocalStorage('SSID')){ //LoggedIn as SuperAdmin
        		$rootScope.loggedInUserId = utilityService.getDataFromLocalStorage('SSID');
        		$scope.userrole = 'superadmin';
                $scope.initializeSuperAdminMode();
                
        }else if(utilityService.getDataFromLocalStorage('CID')){ //LoggedIn as Admin
        		$rootScope.loggedInUserId = utilityService.getDataFromLocalStorage('CID');
                $scope.userrole = 'candidate';
                $scope.initializeAdminMode();
                
        } 

        


        
    }]);


//})(socialApp.controllers);  