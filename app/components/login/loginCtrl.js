//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('loginCtrl',['$scope','$rootScope','utilityService','$state', 
        function($scope,$rootScope,utilityService,$state) {
         console.log("inside loginCtrl");

        window.sessionStorage.clear();
        $rootScope.loggedInUserId = undefined;
        $rootScope.loggedInUserDetails = undefined;
        utilityService.hideWaitLoader();
        addGlobalEventHandler();
        $('.header-user-name').addClass('hide');
        
        $scope.isButtonClicked = false;
        $scope.superAdminFormControl = {
        	//'username':'admin@admin.com',
        	//'password':'Admin@123***',
            'loginAs':'superAdmin'
        };
        $scope.candidateFormControl = {};
        $scope.loginErrMsg = undefined;

        $scope.$watch('superAdminFormControl.loginAs', function(value) {
            console.log(value);
            if(value == 'superAdmin'){
                /*$scope.superAdminFormControl = {
                    'username':'admin@admin.com',
                    'password':'Admin@123***',
                    'loginAs':'superAdmin'
                };*/
            }else{
                /*$scope.superAdminFormControl = {
                    'username':'ganu@g.com',
                    'password':'123',
                    'loginAs':'admin'
                };*/
            }
        });

        


        $scope.superAdminLogin = function(superAdminForm){
           	try{
        			console.log("inside superAdminLogin",$scope.superAdminFormControl);
	        		$scope.isButtonClicked = true;

	        		if(!superAdminForm.$valid){
                        $("[name='" + superAdminForm.$name + "']").find('.ng-invalid:visible:first').focus();
	        			return;
	        		}
                    var postUrl;
                    if($scope.superAdminFormControl.loginAs == "superAdmin"){
                        postUrl = CONSTANTS.BASE_URL + CONSTANTS.SUPER_ADMIN_LOGIN_URL;    
                    }else{ 
                        postUrl = CONSTANTS.BASE_URL + CONSTANTS.CANDIDATE_LOGIN_URL;
                    }

                    utilityService.showWaitLoader();
	        		
	        		var postData = {"method":"login","email":$scope.superAdminFormControl.username,"password":$scope.superAdminFormControl.password};
	        		utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
	        			console.log('inside login success',data.error);
                        utilityService.hideWaitLoader();
                        if($scope.superAdminFormControl.loginAs == "superAdmin"){
                            if(data.status == 'OK'){
                                    if(data.SSID){
                                        utilityService.storeDataToLocalStorage('SSID', data.SSID);
                                    }
                                    //window.location.href = "http://localhost/Dev/freelance/SOCIAL_APP/web_portal_dev/#/dashboard";
                                    $state.transitionTo('dashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);
                                    utilityService.hideWaitLoader();

                            }else{
                                if(data.message){
                                    $scope.loginErrMsg = data.message;
                                    utilityService.hideWaitLoader();
                                }
                                
                            }


                        }else{
                            if(data.status == 'OK'){
                                    if(data.CID){
                                        utilityService.storeDataToLocalStorage('CID', data.CID);
                                        //data.subscription = 'NagariSuvidha';
                                        if(data.subscription == 'Birthday' ){ //Birthday
                                            $state.transitionTo('birthdayOnly',{'cid':data.CID}, CONSTANTS.STATE_TRANS_OPTIONS);
                                        }else if(data.subscription == 'NagariSuvidha' || data.subscription == ''){ //Nagari Suvidha
                                            $state.transitionTo('candidatedashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);
                                        }
                                    }
                                    
                                    utilityService.hideWaitLoader();

                            }else{
                                if(data.message){
                                    $scope.loginErrMsg = data.message;
                                    utilityService.hideWaitLoader();
                                }
                                
                            }

                        }
	        				        			
	        		},function(err){
	        			console.log('inside login error',err);
	        			alert("Something went wrong please try again");
                        utilityService.hideWaitLoader();
	        		});
        		}catch(e){
        			alert("Exeption occured, please try again");
                    utilityService.hideWaitLoader();
        		}
        		
        };

        $scope.openForgotPassPopUp = function(){
            if($scope.superAdminFormControl.loginAs == 'admin'){
                 $scope.userrole = 'candidate';
            }else{
                 $scope.userrole = 'superadmin';
            }
            utilityService.showNsForgotPassAlertMessage($scope);
            
        };

       

    }]);


//})(socialApp.controllers);