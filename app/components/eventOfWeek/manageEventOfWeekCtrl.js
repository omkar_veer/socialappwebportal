//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('manageEventOfWeekCtrl',['$scope','$rootScope','utilityService','isAuthorised','$state', '$stateParams',
        function($scope,$rootScope,utilityService,isAuthorised,$state,$stateParams) {
        console.log("inside manageEventOfWeekCtrl");

        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }
        addGlobalEventHandler();
        var EventOfWeekCtrl = $scope;

        EventOfWeekCtrl.eventOfWeekFormControl = {
            
        };
        EventOfWeekCtrl.dateFormat = 'yyyy-MM-dd';
        EventOfWeekCtrl.minDateLimit = new Date();
        

        EventOfWeekCtrl.sendEventOfTheWeek = function(form){
            try{
                EventOfWeekCtrl.isButtonClicked = true;

                if(!form.$valid){
                    $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
                    return;
                }

                var postUrl;
                postUrl = CONSTANTS.BASE_URL + CONSTANTS.SEND_EVENT_OF_WEEK;    
                utilityService.showWaitLoader();
                
                
                EventOfWeekCtrl.eventOfWeekFormControl['CID'] = utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                
                utilityService.requestPOSTHttpService(postUrl, EventOfWeekCtrl.eventOfWeekFormControl ,function(data){
                    console.log('inside sendEventOfTheWeek success',data);
                    utilityService.hideWaitLoader();
                     $scope.isButtonClicked = false;
                     utilityService.showAlertMessage("Event of the week sent successfully!");
                     form.$setPristine();
                    form.$setUntouched();
                    EventOfWeekCtrl.isButtonClicked = false;
                    EventOfWeekCtrl.eventOfWeekFormControl = {};
                    /*if(data.status == 'OK'){
                          utilityService.showAlertMessage(data.message);
                    }else{
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage(data.message);
                    }*/

                    
                                            
                },function(err){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });
            }catch(ex){
                utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
            }




        }

        

    }]);


//})(socialApp.controllers);