//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('manageAnnouncementCtrl',['$scope','$rootScope','utilityService','isAuthorised','$state', '$stateParams',
    	function($scope,$rootScope,utilityService,isAuthorised,$state,$stateParams) {
        console.log("inside manageAnnouncementCtrl");

        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        var AnnouncemntCtrl = $scope;

        AnnouncemntCtrl.announcementFormControl = {
            'announcementfiletype':'Simple_Text'
        };

        addGlobalEventHandler();

        

        AnnouncemntCtrl.sendAnnouncement = function(form){
        	try{
	        	AnnouncemntCtrl.isButtonClicked = true;

	        	if(!form.$valid){
	        		$("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
	                return;
	            }

	            var postUrl;
	            postUrl = CONSTANTS.BASE_URL + CONSTANTS.SEND_ANNOUNCEMENT;    
	            utilityService.showWaitLoader();
	            
	            
	            AnnouncemntCtrl.announcementFormControl['CID'] = utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate');
	            
	            utilityService.requestFilePOSTHttpService(postUrl, AnnouncemntCtrl.announcementFormControl ,function(data){
	                console.log('inside resolveComplaint success',data);
	                utilityService.hideWaitLoader();
	                 $scope.isButtonClicked = false;
	                 utilityService.showAlertMessage("Announcement sent successfully!");
	                 form.$setPristine();
		            form.$setUntouched();
		            AnnouncemntCtrl.isButtonClicked = false;
		            AnnouncemntCtrl.announcementFormControl = {
            			'announcementfiletype':'Simple_Text'
        			};
	                /*if(data.status == 'OK'){
	                      utilityService.showAlertMessage(data.message);
	                }else{
	                    utilityService.hideWaitLoader();
                	    utilityService.showAlertMessage(data.message);
	                }*/

	                
	                                        
	            },function(err){
	                utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
	            });
			}catch(ex){
				utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
			}




        }

        

    }]);


//})(socialApp.controllers);