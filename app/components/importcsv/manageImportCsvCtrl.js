//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('manageImportCsvCtrl',['$scope','$rootScope','utilityService','isAuthorised','$state', '$stateParams',
    	function($scope,$rootScope,utilityService,isAuthorised,$state,$stateParams) {
        console.log("inside manageImportCsvCtrl");

        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        var ImportCtrl = $scope;

        ImportCtrl.importFormControl = {
            
        };

        /*Candidate List Start Here**/
        ImportCtrl.getCandidateList = function (){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_CANDIDATE;
                    
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getCandidateList success',data);
                    
                    if(data.status == 'OK' && data.candidate){
                            ImportCtrl.candidateList = data.candidate;
                    }else if(data.status == 'OK' && !data.candidate){
                        utilityService.showAlertMessage('There are no candidate,You can add New candidate by clicking on Add New Candidate button');
                           
                    }else if(data.status == 'FAILED'){
                        utilityService.showAlertMessage(data.message);
                    }
                    
                    utilityService.hideWaitLoader();
                },function(err){
                    console.log('inside getCandidateList error',err);
                    //alert("Something went wrong please try again");
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });
            }catch(e){
                console.log('inside getCandidateList error',e);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }   
        }

        

        ImportCtrl.importFile = function(form){
        	try{
	        	ImportCtrl.isButtonClicked = true;

	        	if(!form.$valid){
	        		$("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
	                return;
	            }

	            var postUrl;
	            postUrl = CONSTANTS.BASE_URL + CONSTANTS.IMPORT_CSV;    
	            utilityService.showWaitLoader();
	            
	            
	            //ImportCtrl.importFormControl['CID'] = utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate');
	            
	            utilityService.requestFilePOSTHttpService(postUrl, ImportCtrl.importFormControl ,function(data){
	                console.log('inside importFile success',data);
	                utilityService.hideWaitLoader();
	                 $scope.isButtonClicked = false;
	                
	                if(data.status == 'OK'){
	                    utilityService.showAlertMessage("File uploade successfully!");
	                    utilityService.showAlertMessage(data.message);
	                    form.$setPristine();
			            form.$setUntouched();
			            ImportCtrl.isButtonClicked = false;
			            ImportCtrl.importFormControl = {};
	                }else{
	                    utilityService.hideWaitLoader();
                	    utilityService.showAlertMessage(data.message || data.errorMessage);
	                }

	                
	                                        
	            },function(err){
	                utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
	            });
			}catch(ex){
				utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
			}




        }

        

    }]);


//})(socialApp.controllers);