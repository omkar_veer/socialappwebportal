//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('manageCandidateCtrl',['$scope','$rootScope','utilityService','$state','isAuthorised','$stateParams',
        function($scope,$rootScope,utilityService,$state,isAuthorised,$stateParams) {
        console.log("inside manageCandidateCtrl",$stateParams);
        
        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }
        setTimeout(function(){


            if((window.location.hash.indexOf('#/create-candidate') != -1)){ //Create Mode
                               
            }else{ //Edit / View Mode
                if((window.location.hash.indexOf('#/edit-profile') != -1) 
                    || (window.location.hash.indexOf('#/view-profile') != -1)){
                        $scope.author = "admin";

                }

                if((window.location.hash.indexOf('#/edit-candidate') != -1) 
                    ||(window.location.hash.indexOf('#/view-candidate') != -1)){
                        $scope.author = "superadmin";
                } 

                if(!$stateParams.cid){
                        $scope.author == 'superadmin' ? $state.transitionTo('candidates',{}, CONSTANTS.STATE_TRANS_OPTIONS)
                                                      : $state.transitionTo('candidatedashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);           
                }else{
                    $scope.isEditMode = true;
                    $scope.currentCadidateId = $stateParams.cid;    
                    utilityService.storeDataToLocalStorage('currentCreatedCandidate', $stateParams.cid);
                }  


            }

            $('input,textarea').on('keypress',function(){
                return  rejectSpecialChar();
            });
        },1000);
        var manageCandidate = $scope;
        window.uploadedImage = [];
        $scope.isEditMode = false;
        $scope.pageHeader = "नवीन उमेदवार जतन करा";
        $scope.OTPForm = {};

        $scope.loadingFiles = true;
        $scope.loadingFiles = false;
        $scope.queue = [];
        $scope.candidateFormControl = {};
        $scope.candidateFormControl.profile = {
            'assembly':'Localbody',
            'managedby':'1',
            'gender':'Male',
            'jahirnamastatusTemp':true,
            'profilestatusTemp':true,
            'partystatusTemp':true,
            'workstatusTemp':true
            

        };
        $scope.candidateFormControl.works = {
            'social':[],
            'development':[],
            'religious':[],
            'sport':[]
        };
        $scope.socialworkimage = undefined;
        $scope.candidateFormControl.partyProfile = {};
        $scope.isProfileUndone = true;
        $scope.candidateFormControl.jahirnama = [];

        manageCandidate.emergencyServices = CONSTANTS.HARDCODED_VALUES.EMERGENCY_SERVICES;
        manageCandidate.municipalServices = CONSTANTS.HARDCODED_VALUES.CORPORATION_SERVICES;
        manageCandidate.commercialServices = CONSTANTS.HARDCODED_VALUES.COMMERCIAL_SERVICES;
        manageCandidate.emergencyServiceFormControl = {
            'servicestatus':'1',
            'servicephone':[]
        };
        manageCandidate.municipalServiceFormControl = {
            'servicestatus':'1',
            'servicephone':[]
        };
        manageCandidate.commercialServiceFormControl = {
            'servicestatus':'1',
            'servicephone':[]
        };
        manageCandidate.importantInfoServiceFormControl = {
            'servicestatus':'1',
            'servicephone':[]
        };
        manageCandidate.medicalassistanceServiceFormControl = {
            'servicestatus':'1',
            'servicephone':[]
        };
        manageCandidate.showAddButton = true;

        //Services
        manageCandidate.emergencyservicesform = false;
        manageCandidate.municipalservicesform = false;
        manageCandidate.commercialservicesform = false;
        manageCandidate.importantInfoservicesform = false;
        manageCandidate.medicalassistanceservicesform = false;
        manageCandidate.emergencyServicesArray = [];
        manageCandidate.municipalServicesArray = [];
        manageCandidate.commercialServicesArray = [];
        manageCandidate.importantInfoServicesArray = [];
        manageCandidate.medicalassistanceServicesArray = [];

        //Helping Hand Categories
        manageCandidate.newbusinessmenform = false;
        manageCandidate.skilldevelopmentform = false;
        manageCandidate.governmentloanschemesform = false;
        manageCandidate.utilitybillsform = false;
        manageCandidate.pmcschemesform = false;
        manageCandidate.newBusinessmenFormControl = {
            'hhstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.skillDevelopmentFormControl = {
            'hhstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.govtLoanSchemesFormControl = {
            'hhstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.pmcSchemesFormControl = {
            'hhstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.utilityBillsFormControl = {
            'hhstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.showHHAddButton = true;
        manageCandidate.newBusinessMenListArray = [];
        manageCandidate.skillDevelopmentListArray = [];
        manageCandidate.govtLoanSchemesListArray = [];
        manageCandidate.pmcSchemesListArray = [];
        manageCandidate.utilityBillsListArray = [];
        
        //Matdar Jagruti Tab
        manageCandidate.matdarJagrutiFormControl = {
            'mjstatus':'1',
            'uploadtype':'Url'
        };
        manageCandidate.showMatdarJagrutiAddButton = true;
        manageCandidate.matdarjagrutiform = false;
        manageCandidate.matdarJagrutiListArray = [];

        //Social Net Tab
        manageCandidate.socialNetFormControl = {
            'status':'1',
            
        };
        manageCandidate.showSocailNetAddButton = true;
        manageCandidate.socialnetform = false;
        manageCandidate.socailNetListArray = [];
        manageCandidate.isPhoneAddButtonClicked = false;


        


        var date = new Date()
        date.setMonth(date.getMonth() - 216);
        $scope.maxDateLimit = date;
        $scope.minDateLimit = new Date('1900/1/1');
        $scope.dateFormat = 'yyyy-MM-dd';
        //$scope.dateFormat = 'dd/MM/yyyy';

        $(document).ready(function(){
        	(function() {

		          [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
		            new CBPFWTabs( el );
		          });

        	})();

        });

        

        $scope.$watch('candidateFormControl.profile.subscription', function(value) {
             if(value == 'NagariSuvidha'){
                setTimeout(function(){
                    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
                       new CBPFWTabs( el );
                    });
                },2000);
                     
             }
        });

        /*$scope.$on('profilepic', function(imageData){
             $scope.candidateFormControl.profile['image']  = imageData;
        });

        $scope.$on('partypic', function(imageData){
            $scope.candidateFormControl.profile['partyimage'] = imageData;
        });*/

        $scope.$on('OTPVerified', function(event,data){
            $scope.isProfileUndone = false;
            if($scope.candidateFormControl.profile.subscription == 'Birthday'){
                 $state.transitionTo('candidates',{}, CONSTANTS.STATE_TRANS_OPTIONS)
            }
        });
        
        $scope.showImagePreview = {
            'social':false,
            'development':false,
            'sport':false,
            'religious':false,
            'jahirnama':false
        };

        $scope.imageSelectorClickHandler = function(type){
            console.log('imageSelectorClickHandler');
            $scope.showImagePreview[type] = true;
            $scope.jahirnamaimage = '';
            $scope.socialworkimage = '';
            $scope.devworkimage = '';
            $scope.religiousworkimage = '';
            $scope.sportworkimage = '';
        }

        $scope.saveJahirnama = function(image, desc){
            //loadImageFileAsURL(image,function(imageData){
                    //console.log("image data",imageData);
                    
                    try{
                        utilityService.showWaitLoader();
                        var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SAVE_JAHIRNAMA_WORKS;
                        var postData = {
                                        "CID" : utilityService.getDataFromLocalStorage('currentCreatedCandidate') || 1,
                                        "image":image,
                                        "description":desc,
                                        "type":"jahirnama",
                                        "worktype":""
                                       };
                        utilityService.requestPOSTHttpService(postUrl,postData  ,function(data){
                            console.log('inside saveJahirnama success',data);
                            
                            if(data.status == 'OK'){
                                    var obj = {
                                        "CID" : utilityService.getDataFromLocalStorage('currentCreatedCandidate') || 1,
                                        "image":image,
                                        "description":desc,
                                        "type":"jahirnama",
                                        "worktype":"",
                                        "galleryid":data.galleryid
                                    };

                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                            utilityService.hideWaitLoader();   
                                           utilityService.showAlertMessage("Jahirnama image & description added");
                                           $scope.$$childTail.jahirnamaimage = undefined;
                                           $scope.showImagePreview['jahirnama'] = false;
                                           $scope.candidateFormControl.jahirnama.unshift(obj);
                                           //$scope.candidateFormControl.jahirnama.push(obj);
                                          
                                        });
                                    }, 500);


                            }else{
                                utilityService.hideWaitLoader();
                            }
                            
                        },function(err){
                            console.log('inside saveJahirnama error',err);
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                            
                        });
                

                    }catch(e){
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                    }
                   
                    


            //});

        }

        $scope.deleteJahirnamaOrWork = function(obj,type,objtype){
                try{
                        utilityService.showWaitLoader();
                        var postUrl = CONSTANTS.BASE_URL + CONSTANTS.DELETE_JAHIRNAMA_WORK;
                        var postData = {
                                        "CID" : utilityService.getDataFromLocalStorage('currentCreatedCandidate') || 1,
                                        "galleryid":obj.galleryid
                                       };
                        utilityService.requestPOSTHttpService(postUrl,postData  ,function(data){
                            console.log('inside deleteJahirnamaOrWork success',data);
                            
                            if(data.status == 'OK'){
                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                           utilityService.hideWaitLoader();   
                                           utilityService.showAlertMessage(data.message);
                                           if(type == 'jahirnama'){
                                             $scope.candidateFormControl.jahirnama =  _.reject($scope.candidateFormControl.jahirnama, function(d){ return d.galleryid === obj.galleryid; });
                                           }else{
                                              $scope.candidateFormControl.works[objtype] = _.reject($scope.candidateFormControl.works[objtype], function(d){ return d.galleryid === obj.galleryid; });
                                           }
                                           
                                        });
                                    }, 500);


                            }else{
                                utilityService.hideWaitLoader();
                            }
                            
                        },function(err){
                            console.log('inside deleteJahirnama error',err);
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                            
                        });
                

                    }catch(e){
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                    }
        }

        $scope.saveWorkDetails = function(image, desc, worktype, objtype){
            //loadImageFileAsURL(image,function(imageData){
                    
                    try{
                        utilityService.showWaitLoader();
                        var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SAVE_JAHIRNAMA_WORKS;
                        var postData = {
                                        "CID" : utilityService.getDataFromLocalStorage('currentCreatedCandidate') || 1,
                                        "image":image,
                                        "description":desc,
                                        "type":"work",
                                        "worktype":worktype
                                       };
                        utilityService.requestPOSTHttpService(postUrl,postData  ,function(data){
                            console.log('inside saveWorkDetails success',data);
                            
                            if(data.status == 'OK'){
                                    var obj = {
                                        "CID" : utilityService.getDataFromLocalStorage('currentCreatedCandidate') || 1,
                                        "image":image,
                                        "description":desc,
                                        "type":"work",
                                        "worktype":worktype,
                                        "galleryid":data.galleryid
                                    };

                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                            utilityService.hideWaitLoader();   
                                           utilityService.showAlertMessage("Work image & description added");
                                           $scope.socialworkimage = undefined;
                                           $scope.showImagePreview[ objtype ] = false;
                                           $scope.candidateFormControl.works[ objtype ].unshift(obj);
                                           //$scope.candidateFormControl.jahirnama.push(obj);
                                          
                                        });
                                    }, 500);


                            }else{
                                utilityService.hideWaitLoader();
                            }
                            
                        },function(err){
                            console.log('inside saveJahirnama error',err);
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                            
                        });
                

                    }catch(e){
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage("Something went wrong please try again");
                    }
                   
                    


            //});

        }

        
        

        

        
        $scope.saveCandidateProfile = function(candidateprofileform){
            try{
                    console.log("inside candidate profile save",$scope.candidateFormControl);
                    $scope.isButtonClicked = true;
                    $scope.alertMsg = "Regi Success";
                    
                    
                    

                    if(!candidateprofileform.$valid){
                        $("[name='" + candidateprofileform.$name + "']").find('.ng-invalid:visible:first').focus();
                        return;
                    }
                    if(!$scope.candidateFormControl.profile.imageTemp){
                        utilityService.showAlertMessage("Please upload profile picture");
                         return;
                    }
                    if($scope.candidateFormControl.profile.partyImageTemp == 'noimage'){
                        utilityService.showAlertMessage("Please upload party picture");
                         return;
                    }
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CREATE_CANDIDATE_URL;
                    if($scope.isEditMode){
                        postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_PRPOFILE_TAB;
                        $scope.candidateFormControl.profile['CID'] = utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                    }
                    $scope.candidateFormControl.profile['SSID'] = utilityService.getDataFromLocalStorage('SSID');//"9";
                    //$scope.candidateFormControl.profile['image'] = window.uploadedImage['profilepic']  ||  $scope.candidateFormControl.profile['image'];
                    //$scope.candidateFormControl.profile['partyimage'] = window.uploadedImage['partypic'] || $scope.candidateFormControl.profile['partyimage'];
                    
                    //if($scope.candidateFormControl.profile.profilestatusTemp == true || $scope.candidateFormControl.profile.profilestatusTemp == false){
                        $scope.candidateFormControl.profile.profilestatus = $scope.candidateFormControl.profile.profilestatusTemp == true ? "1" :"0";
                    //}else{
                      //  $scope.candidateFormControl.profile.profilestatus = $scope.candidateFormControl.profile.profilestatusTemp;                        
                    //}

                    //if($scope.candidateFormControl.profile.partystatusTemp == true || $scope.candidateFormControl.profile.partystatusTemp == false){
                        $scope.candidateFormControl.profile.partystatus = $scope.candidateFormControl.profile.partystatusTemp == true ? "1" :"0";
                    //}else{
                      //  $scope.candidateFormControl.profile.partystatus = $scope.candidateFormControl.profile.partystatusTemp;                        
                    //}

                    //if($scope.candidateFormControl.profile.workstatusTemp == true || $scope.candidateFormControl.profile.workstatusTemp == false){
                        $scope.candidateFormControl.profile.workstatus = $scope.candidateFormControl.profile.workstatusTemp == true ? "1" :"0";
                    //}else{
                      //  $scope.candidateFormControl.profile.workstatus = $scope.candidateFormControl.profile.workstatusTemp;                        
                    //}

                    //if($scope.candidateFormControl.profile.jahirnamastatusTemp == true || $scope.candidateFormControl.profile.jahirnamastatusTemp == false){
                        $scope.candidateFormControl.profile.jahirnamastatus = $scope.candidateFormControl.profile.jahirnamastatusTemp == true ? "1" :"0";
                    //}else{
                      //  $scope.candidateFormControl.profile.jahirnamastatus = $scope.candidateFormControl.profile.jahirnamastatusTemp;                        
                    //}

                    //$scope.candidateFormControl.profile.servicesstatus =  $scope.candidateFormControl.profile.servicesstatus == true ? "1" :"0";
                    //$scope.candidateFormControl.profile.mjstatus =  $scope.candidateFormControl.profile.mjstatus == true ? "1" :"0";
                    //$scope.candidateFormControl.profile.helpinghandstatus =  $scope.candidateFormControl.profile.helpinghandstatus == true ? "1" :"0";
                    //$scope.candidateFormControl.profile.socialstatus =  $scope.candidateFormControl.profile.socialstatus == true ? "1" :"0";
                    console.log("BEFORE PROFILE SAVE",$scope.candidateFormControl);
                    //return;
                    
                    utilityService.requestPOSTHttpService(postUrl, $scope.candidateFormControl.profile ,function(data){
                        console.log('inside create candidate success',data);
                        //$scope.locationMasterData = data;
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                        if(!$scope.isEditMode){ //Create Mode
                            if(typeof data == 'string'){
                                var splitedStr = data.split('{') //[1];
                                if(splitedStr.length != 0){
                                        //splitedStr[1];
                                        var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

                                        if(strRes.status == 'OK'){
                                    
                                            //$scope.isProfileUndone = false;
                                            if(strRes.CID){
                                                $scope.OTPForm.phoneNumber = $scope.candidateFormControl.profile.phone;
                                                $scope.OTPForm.CID = strRes.CID;
                                                $scope.OTPForm.type = 'candidate';
                                                utilityService.showOTPAlertMessage($scope);
                                                utilityService.storeDataToLocalStorage('currentCreatedCandidate', strRes.CID);
                                            }

                                        }else{
                                            utilityService.showAlertMessage(strRes.message);

                                        }

                                }else{
                                    utilityService.showAlertMessage("Something went wrong please try again");
                                }
                            }else{
                                if(data.status == 'FAILED'){
                                    /*if(data.otp == 'pending'){

                                    }else{*/
                                        utilityService.showAlertMessage(data.message);  
                                    //}
                                    

                                }
                            }
                        }else{  //Edit Mode
                               
                                if(typeof data == 'string'){
                                var splitedStr = data.split('{')
                                if(splitedStr.length != 0){
                                        
                                        var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

                                        if(strRes.status == 'OK'){
                                    
                                           utilityService.showAlertMessage(strRes.message);

                                        }else{
                                            utilityService.showAlertMessage(strRes.message);

                                        }

                                }else{
                                    utilityService.showAlertMessage("Something went wrong please try again");
                                }
                            }else{
                                if(data.status == 'FAILED'){
                                    utilityService.showAlertMessage(data.message);  
                                }
                            }



                        }
                        
                        
                    },function(err){
                        console.log('inside create candidate error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }

        $scope.savePartyDetails = function(candidatepartyprofileform){
            try{
                    console.log("inside candidate profile save",$scope.candidateFormControl.partyProfile);
                    $scope.isButtonClicked = true;

                    if(!candidatepartyprofileform.$valid){
                            $("[name='" + candidatepartyprofileform.$name + "']").find('.ng-invalid:visible:first').focus();
                            return;
                    }
                    utilityService.showWaitLoader();
                    $scope.candidateFormControl.partyProfile['CID'] =  utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                    $scope.candidateFormControl.partyProfile['partydescription'] =  $scope.candidateFormControl.partyProfile['partydetails'] ;
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SAVE_PARTY_DETAILS_URL;
                    
                    utilityService.requestPOSTHttpService(postUrl, $scope.candidateFormControl.partyProfile ,function(data){
                        console.log('inside partyProfile success',data);
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                        if(data.status == 'OK'){
                            utilityService.showAlertMessage(data.message);
                        }else{
                            utilityService.showAlertMessage(data.message);

                        }

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }

        /*$scope.saveCandidateJahirnama = function(candidatejahirnamaform){
            try{
                    console.log("inside candidatejahirnamaform save",$scope.candidateFormControl.jahirnama);
                    $scope.isButtonClicked = true;

                    if(!candidatejahirnamaform.$valid){
                            return;
                    }
                    utilityService.showWaitLoader();
                    $scope.candidateFormControl.jahirnama['CID'] =  utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                    $scope.candidateFormControl.jahirnama['image1'] =  window.uploadedImage['jahirnama_image1'] || $scope.candidateFormControl.jahirnama['image1'];
                    $scope.candidateFormControl.jahirnama['image2'] =  window.uploadedImage['jahirnama_image2'] || $scope.candidateFormControl.jahirnama['image2'];
                    $scope.candidateFormControl.jahirnama['image3'] =  window.uploadedImage['jahirnama_image3'] || $scope.candidateFormControl.jahirnama['image3'];
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SAVE_JAGIRNAMA_DETAILS_URL;
                    //return;

                    utilityService.requestPOSTHttpService(postUrl, $scope.candidateFormControl.jahirnama ,function(data){
                        console.log('inside candidatejahirnamaform success',data);
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                        if(data.status == 'OK'){
                            utilityService.showAlertMessage(data.message);
                        }else{
                            utilityService.showAlertMessage(data.message);

                        }

                    },function(err){
                        console.log('inside candidatejahirnamaform error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }


        
        $scope.saveCandidateWork = function(formName,tabName,tabId){
            try{
                    console.log("inside saveCandidateWork save",$scope.candidateFormControl.works);
                    $scope.isButtonClicked = true;

                    if(!formName.$valid){
                            return;
                    }

                    utilityService.showWaitLoader();
                    $scope.candidateFormControl.works[tabId]['CID'] =  utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                    $scope.candidateFormControl.works[tabId]['image1'] =  window.uploadedImage[tabId+'_image1'] || $scope.candidateFormControl.works[tabId]['image1'];
                    $scope.candidateFormControl.works[tabId]['image2'] =  window.uploadedImage[tabId+'_image2'] || $scope.candidateFormControl.works[tabId]['image2'];
                    $scope.candidateFormControl.works[tabId]['image3'] =  window.uploadedImage[tabId+'_image3'] || $scope.candidateFormControl.works[tabId]['image3'];
                    $scope.candidateFormControl.works[tabId]['status'] =  $scope.candidateFormControl.profile.workstatus;
                    $scope.candidateFormControl.works[tabId]['worktype'] =  tabName;
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SAVE_WORK_DETAILS_URL;
                    //return;

                    if($scope.isEditMode){
                        postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_WORK_TAB;
                    }

                    utilityService.requestPOSTHttpService(postUrl, $scope.candidateFormControl.works[tabId] ,function(data){
                        console.log('inside saveCandidateWork success',data);
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                        if(data.status == 'OK'){
                            utilityService.showAlertMessage(data.message);
                        }else{
                            utilityService.showAlertMessage(data.message);

                        }

                    },function(err){
                        console.log('inside saveCandidateWork error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }*/

        $scope.getCastList = function(){
            
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_CAST_LIST;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getCastList success',data);
                    
                    if(data.status == 'OK' && data.caste){
                        $scope.casteList = data.caste;
                    }else{
                        console.log('inside getCastList error',data);
                    }
                    
                },function(err){
                    console.log('inside getCastList error',err);
                    
                });
            }catch(e){

            }
        }

        $scope.getAllStates = function(){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_STATES;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.states){
                        $scope.states = data.states;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.states = ["Maharashtra"];

        }

        $scope.getDistricts = function(state){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_DISTRICTS;
                var postParam =  {"state":state};
                utilityService.requestPOSTHttpService(postUrl,postParam,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.districts){
                        $scope.districts = data.districts;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){
                    console.log(e);
            }
            //$scope.districts = ["Pune"];

        }

        $scope.getTalukas = function(district){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_TALUKAS;
                var postParam =  {"district": district};

                utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.talukas){
                        $scope.talukas = data.talukas;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.talukas = ["Haveli"];

            $scope.getLokSabha(district);

        }


        $scope.getPincodes = function(taluka){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_PINCODES;
                var postParam =  {"taluka":taluka};
                utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.pincodes){
                        $scope.pincodes = data.pincodes;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.pincodes = ["412105"];

        }

        $scope.getLokSabha = function(district){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_LOKSABHAS;
                var postParam =  {"district":district};
                utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.loksabha){
                        $scope.loksabhas = data.loksabha;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.loksabhas = ["Pune"];
        }

        $scope.getVidhanSabhas = function(loksabha){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_VIDHASABHAS;
                var postParam =  {"loksabha":loksabha};
                utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.vidhansabha){
                        $scope.vidhansabhas = data.vidhansabha;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.vidhansabhas = ["Shivajinagar"];
        }

        $scope.getLocalAssemblyWards = function(vidhansabha){
            /*try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_LOKSABHA;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.loksabhas){
                        $scope.loksabhas = data.loksabhas;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }*/
            $scope.localWards = ["25","26"];
        }

        $scope.setPartyImage = function(partyId){
              var selectedParty = _.filter($scope.politicalPartyList, function(obj){ 
                    return ( obj['partyid'] == partyId);
              });
              if(selectedParty.length > 0){
                    $scope.candidateFormControl.profile.partyImageTemp  = selectedParty[0].partysymbol;
                    
                    $scope.candidateFormControl.partyProfile['partyimage']  = $scope.candidateFormControl.profile['partyImageTemp'];
                    $scope.candidateFormControl.partyProfile['partyname'] = selectedParty[0].partyname;
                    //$scope.selectedPoliticalParty = selectedParty[0];

              }else{
                    $scope.candidateFormControl.profile.partyImageTemp  = "noimage";

                    $scope.candidateFormControl.partyProfile['partyimage']  = $scope.candidateFormControl.profile['partyImageTemp'];
                    $scope.candidateFormControl.partyProfile['partyname'] = "Others";

                    

                    
              }

        }

        $scope.getPoliticalPartyList  =function(){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_PARTY_LIST;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.partydetails){
                        $scope.politicalPartyList = data.partydetails;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    //alert("Something went wrong please try again");
                });
            }catch(e){

            }

        }
        

        $scope.getLocationMaster = function(){
            try{
                utilityService.hideWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.LOCATION_MASTER_URL;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getLocationMaster success',data);
                    
                    if(data.status == 'OK' && data.location){
                            $scope.locationMasterData = data.location;
                    }else{
                        console.log('inside location Master error',data);
                    }
                    
                },function(err){
                    console.log('inside location Master error',err);
                    //alert("Something went wrong please try again");
                });
            }catch(e){

            }

        }

        $scope.getWardInfo = function(pin){
            if(pin){
                try{
                    
                    var filteredLocation = _.where($scope.locationMasterData, {"pincode":pin});
                    if(filteredLocation.length != 0){
                            $scope.candidateFormControl.profile.dist = filteredLocation[0].district;
                            $scope.candidateFormControl.profile.state = filteredLocation[0].state;
                            $scope.candidateFormControl.profile.tal = filteredLocation[0].taluka;
                    }
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_WARD_DETAILS;
                    var postData = {"pincode":pin};
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside getWardInfo success',data);
                        
                        if(data.status == 'OK' && data.warddetails){
                                $scope.wardDetails = data.warddetails;
                        }else{
                            console.log('inside getWardInfo  error',data);
                        }
                        
                    },function(err){
                        console.log('inside getWardInfo  error',err);
                        //alert("Something went wrong please try again");
                    });
                }catch(e){

                }


            }
        }

        $scope.showVisibleTabOption = false;
        $scope.$watch('candidateFormControl.profile.subscription', function(value) {
             if(value == 'Birthday'){
                    $scope.showVisibleTabOption = false;
             }else if(value == 'NagariSuvidha'){
                    $scope.showVisibleTabOption = true;
             }
        });

        $scope.getReligionList=  function(){
            utilityService.getReligionList(function(data){
                $scope.religionList = data;
            },function(){

            });
        }


        $scope.$watch('candidateFormControl.profile.assembly', function(value) {
            console.log(value);
            if(value == 'Loksabha'){
                $scope.isLoksabhaActive = true;
                $scope.isVidhanSabhaActive = false;
                $scope.isLocalBodyActive = false;
            }else if(value == 'Vidhansabha'){
                $scope.isLoksabhaActive = true;
                $scope.isVidhanSabhaActive = true;
                $scope.isLocalBodyActive = false;
            }else {
                $scope.isLoksabhaActive = true;
                $scope.isVidhanSabhaActive = true;
                $scope.isLocalBodyActive = true;
            }
        });


        $scope.tabClickHandler = function(tabid,elem){
            console.log("tabid clicked",elem);
            $scope.isButtonClicked = false;
            if($stateParams.cid){  //Edit / View Mode
                if((window.location.hash.indexOf('#/view-candidate') != -1) 
                || (window.location.hash.indexOf('#/view-profile') != -1)){
                    $scope.getCandidateData(tabid,'view');
                }else if((window.location.hash.indexOf('#/edit-candidate') != -1) 
                ||(window.location.hash.indexOf('#/edit-profile') != -1)){
                    $scope.getCandidateData(tabid,'edit');    
                }
                
            }else{
                if($(elem.currentTarget).attr('locked') == 'true'){
                    utilityService.showAlertMessage("Please create candidate & complete your registration process by verifying your mobile number!!!");
                }
            }
        }

        $scope.initProfileTabInEditMode = function(){
            $scope.getDistricts($scope.candidateFormControl.profile.state);
            $scope.getTalukas($scope.candidateFormControl.profile.district);
            $scope.getPincodes($scope.candidateFormControl.profile.taluka);
            $scope.getVidhanSabhas($scope.candidateFormControl.profile.loksabha);
            $scope.candidateFormControl.profile.imageTemp = $scope.candidateFormControl.profile.image;
            if($scope.candidateFormControl.profile.partyid == '9'){
                $scope.candidateFormControl.profile.partyImageTemp = $scope.candidateFormControl.profile.otherpartyimage;
            }else{
                $scope.candidateFormControl.profile.partyImageTemp = $scope.candidateFormControl.profile.partyimage;    
            }
            
            $scope.candidateFormControl.profile.partyname = $scope.candidateFormControl.profile.partyid;
            $scope.isProfileUndone = false;

            $scope.candidateFormControl.profile.profilestatusTemp = $scope.candidateFormControl.profile.profilestatus == '1' ? true : false;
            $scope.candidateFormControl.profile.partystatusTemp =  $scope.candidateFormControl.profile.partystatus == '1' ? true : false;
            $scope.candidateFormControl.profile.workstatusTemp =  $scope.candidateFormControl.profile.workstatus == '1' ? true : false;
            $scope.candidateFormControl.profile.jahirnamastatusTemp =  $scope.candidateFormControl.profile.jahirnamastatus == '1' ? true : false;

        }

        $scope.initWorkTabInEditMode = function (work){

            $scope.candidateFormControl.works['social'] = _.filter(work, function(obj){ 
                return ( obj['worktype'] == 'Social Work');
            });

            $scope.candidateFormControl.works['development'] = _.filter(work, function(obj){ 
                return ( (obj['worktype'] == 'Development Work') || (obj['worktype'] == 'Developement Work'));
            });

            $scope.candidateFormControl.works['religious'] = _.filter(work, function(obj){ 
                return ( obj['worktype'] == 'Religious Work');
            });

            $scope.candidateFormControl.works['sport'] = _.filter(work, function(obj){ 
                return ( obj['worktype'] == 'Sport Work');
            });
            
        }

        $scope.getJahirnamaOrWorkData = function(tabid,mode){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_JAHIRNAMA_WORK;
                var postParam = {'CID':parseInt($stateParams.cid) ,'type':tabid, 'worktype':""};
                  //CID, type, worktype
                   utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                        console.log('inside getJahirnamaOrWorkData success',data);
                        utilityService.hideWaitLoader();
                        if(data.status == 'OK'){
                            if(tabid == 'jahirnama'){
                                $scope.candidateFormControl.jahirnama = data.jahirnama;
                            }else if(tabid == 'work'){
                                $scope.initWorkTabInEditMode(data.work);
                            }
                            
                        }
                    },function(err){
                        utilityService.hideWaitLoader();
                    });
            }catch(e){
                utilityService.hideWaitLoader();
            }
            
        }


        $scope.getCandidateData = function(tabid,mode){
                
            try{
                utilityService.showWaitLoader();
                if(tabid == 'profile' || tabid == 'partyprofile'){ 
               
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_CANDIDATE_DATA;
                    var postParam = {'CID':parseInt($stateParams.cid) ,'tabid':tabid};
                    utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                        console.log('inside getCandidateData success',data);
                        utilityService.hideWaitLoader();
                        
                        if(data.status == 'OK'){
                                switch(tabid){
                                    case 'profile':
                                                if(data.profile.length != 0) {
                                                    $scope.candidateFormControl.profile = data.profile[0];
                                                    if($scope.candidateFormControl.profile.partyid == '9'){
                                                        $scope.candidateFormControl.profile.partyImageTemp = $scope.candidateFormControl.profile.otherpartyimage;
                                                    }else{
                                                        $scope.candidateFormControl.profile.partyImageTemp = $scope.candidateFormControl.profile.partyimage;    
                                                    }
                                                    if(mode == 'edit'){
                                                        $scope.initProfileTabInEditMode();
                                                        
                                                    }  
                                                }
                                                break;
                                    
                                    case 'partyprofile':
                                                if(data.partyprofile.length != 0) {
                                                    $scope.candidateFormControl.partyProfile = data.partyprofile[0];   
                                                }
                                                break;
                                    

                                }
                                
                        }else{
                            console.log('inside getCandidateData error',data);
                            utilityService.showAlertMessage(data.message);
                            $scope.author == 'superadmin' ? $state.transitionTo('candidates',{}, CONSTANTS.STATE_TRANS_OPTIONS)
                                                      : $state.transitionTo('candidatedashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);           

                        }
                        
                    },function(err){
                        console.log('inside getCandidateData error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                        //alert("Something went wrong please try again");
                    });
                }else if(tabid ==  'services'){
                    //Default call for Emergency service accordion
                    manageCandidate.getAllServices( 'Emergency Services', parseInt($stateParams.cid) , 'emergencyServicesArray');
                }else if(tabid == 'jahirnama' || tabid == 'work'){ //For Jahirnama And Work Gallery
                    $scope.getJahirnamaOrWorkData(tabid,mode);
                }else if(tabid == 'helpinghand'){
                    utilityService.hideWaitLoader();
                    //Default call for Helping Hand accordion
                    manageCandidate.getAllHelpingHandData( 'New Businessmen', parseInt($stateParams.cid) , 'newBusinessMenListArray');
                }else if(tabid == 'matdarjagruti'){
                    utilityService.hideWaitLoader();
                    manageCandidate.getAllMatdarJagrutiData( 'Matdar Jagruti', parseInt($stateParams.cid) , 'matdarJagrutiListArray');
                }else if(tabid == 'socialnet'){
                    utilityService.hideWaitLoader();
                    manageCandidate.getAllSocialNetData( 'Social Network', parseInt($stateParams.cid) , 'socailNetListArray');
                }
            }catch(e){

            }
        }

        $scope.goToEditCandidate = function(){
            if($stateParams.cid){
                    $state.transitionTo('editcandidate',{'cid':$stateParams.cid}, CONSTANTS.STATE_TRANS_OPTIONS);
            }else{
                    $state.transitionTo('candidates',{}, CONSTANTS.STATE_TRANS_OPTIONS);
            }
        }

        $scope.goToEditProfile = function (){
            if($stateParams.cid){
                    $state.transitionTo('editprofile',{'cid':$stateParams.cid}, CONSTANTS.STATE_TRANS_OPTIONS);
            }else{
                    $state.transitionTo('candidatedashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);
            }
        }

        setTimeout(function(){
            if((window.location.hash.indexOf('#/view-candidate') != -1) 
                || (window.location.hash.indexOf('#/view-profile') != -1)){
                    $scope.getCandidateData('profile','view');

            }

            if((window.location.hash.indexOf('#/edit-candidate') != -1) 
                ||(window.location.hash.indexOf('#/edit-profile') != -1)){
                    $scope.getCandidateData('profile','edit');
                    $scope.isEditMode = true;
                    $scope.pageHeader = "उमेदवार संपादित करा";
            }

        },1000); 


        /*Serivce Tab Section Starts*/
        manageCandidate.enableServiceForm = function(type , formControlName){
            manageCandidate[type] = true;
            manageCandidate.showAddButton = false;
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControlName] = {'servicestatus':'1','servicephone':[]};
        
            
        }

        manageCandidate.disableServiceForm = function(type){
            manageCandidate[type] = false;
            manageCandidate.showAddButton = true;
        }

        manageCandidate.clearFormData = function(form ,  formControl){
            form.$setPristine();
            form.$setUntouched();
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControl] = {'servicestatus':'1' , 'servicephone':[]};
        }

        manageCandidate.disableAllServiceForms = function(){
            var serviceforms = ['medicalassistanceservicesform','municipalservicesform','importantInfoservicesform',
                                'emergencyservicesform','commercialservicesform'
            ];

            for(var i=0;i<serviceforms.length;i++){
                manageCandidate.disableServiceForm(serviceforms[i]);
            }
        }

        manageCandidate.accordionClickHandler = function(category,dataArray,evt){
            manageCandidate.disableAllServiceForms();
            if($(evt.target).parent().parent().attr('aria-expanded') == "false"){
                manageCandidate.getAllServices( category, parseInt($stateParams.cid) , dataArray );    
            }
            
        }

        manageCandidate.goToeditService = function(serviceObj , formControl, formname , elem , constantArray){
            manageCandidate[formControl] = angular.copy(serviceObj);
            manageCandidate[formControl]['filedata'] = manageCandidate[formControl]['serviceimage'];
            var matchedItem = _.filter(manageCandidate[constantArray], function(obj){
                return (manageCandidate[formControl].servicename == obj)
            });
            
            if(matchedItem.length == 0){
                if(manageCandidate[formControl]['category'] == "Important Information" || manageCandidate[formControl]['category'] == "Medical Assistance Services"){
                    
                }else{
                    manageCandidate[formControl]['customservicename'] = manageCandidate[formControl]['servicename'];
                    manageCandidate[formControl]['servicename'] = 'Other';    
                }
                
            }
            
            manageCandidate.enableServiceForm(formname);
            setTimeout(function(){
                 $("[name='servicename']").focus();
             },500);
            $("[name='" + formname + "']").find("[name='servicename']").focus();
            $('html, body').animate({
                 scrollTop: $("#"+ elem).offset().top
            }, 500);
        }

        manageCandidate.getAllServices = function(category,CID,serviceName){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_SERVICES;
                var postData = {'category':category , 'CID':CID };
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("Services fetched from server",data);
                        if(data.servicetype && data.servicetype.length > 0){
                            manageCandidate[serviceName] = data.servicetype;    
                        }else{
                            manageCandidate[serviceName] = [];
                        }
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                //utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.updateServiceStatus = function(status,category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_SERVICE;
                var postData = angular.copy(serviceObj);
                postData['CID'] = serviceObj.candidateid;
                postData['servicestatus'] = status; //((serviceObj.servicestatus == 1) ? 0 : 1);
               /* var postData = {
                    'servicecategoryid':service.servicecategoryid,
                    'CID':service.candidateid,
                    'servicestatus':((service.servicestatus == 1) ? 0 : 1)
                }*/
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                         if(data.status == 'OK'){
                            utilityService.hideWaitLoader();
                            manageCandidate.getAllServices( category, parseInt($stateParams.cid) , listArray);

                        }else{
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage(data.message);    
                        }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.addServiceContactNumber = function(formControl,form){
            manageCandidate.isPhoneAddButtonClicked = true;
            if(!form.tempservicephone.$valid){
                return;
            }
            setTimeout(function(){
                $scope.$apply(function () {
                    manageCandidate[formControl]['servicephone'].push(manageCandidate[formControl]['tempservicephone']);
                    manageCandidate[formControl]['tempservicephone'] = '';
                    manageCandidate.isPhoneAddButtonClicked = false;
                });
            });

        }

        manageCandidate.removeServiceContactNumber = function(formControl,index){
            manageCandidate[formControl]['servicephone'].splice(index,1);
            
            setTimeout(function(){
                $scope.$apply();
            });

        }

        manageCandidate.addServiceDetails = function(form,category,formControl,listArray){
            try{
                manageCandidate.isButtonClicked = true;
                if(!form.$valid){
                    if(form.tempservicephone.$invalid && manageCandidate[formControl].servicephone.length > 0){
                            //Proceed
                    }else{
                       $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
                       return; 
                    }
                    
                }
                //return;
                utilityService.showWaitLoader();
                if(manageCandidate[formControl].servicecategoryid){ //Edit Mode
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_SERVICE;
                }else{
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.ADD_NEW_SERVICE;
                }
                
                manageCandidate[formControl]['CID'] = utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                manageCandidate[formControl]['category'] = category;
                if(manageCandidate[formControl].servicename == 'Other'){
                    manageCandidate[formControl].servicename = manageCandidate[formControl].customservicename;
                }
                //return;
                utilityService.requestPOSTHttpService(postUrl, manageCandidate[formControl] ,function(data){
                     console.log("inside addServiceDetails success",data);
                     if(data.status == 'OK'){
                         utilityService.showAlertMessage(data.message);
                         utilityService.hideWaitLoader();
                         manageCandidate.clearFormData(form , formControl);
                         if(category == 'Important Information'){
                            manageCandidate.disableServiceForm('importantInfoservicesform');
                         }else{
                            manageCandidate.disableServiceForm(category.split(' ').join('').toLowerCase() + 'form');   
                         }
                         
                         manageCandidate.getAllServices( category, parseInt($stateParams.cid) , listArray);
                     }else{
                        utilityService.showAlertMessage(data.message);
                     }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });
            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.deleteServiceCategory = function(category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.DELETE_SERVICES;
                var postData = {'servicecategoryid':serviceObj.servicecategoryid};
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("Service Deleted",data);
                        manageCandidate.getAllServices( category, parseInt($stateParams.cid) , listArray);
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        /*Serivce Tab Section Ends*/

        /*Helping Hand Category Start*/

        manageCandidate.enableHelpingHandForm = function(type , formControlName){
            manageCandidate[type] = true;
            manageCandidate.showHHAddButton = false;
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControlName] = {'hhstatus':'1','uploadtype':'Url'};
            
        }

        manageCandidate.disableAllHelpingHandForms = function(){
            var serviceforms = ['newbusinessmenform','skilldevelopmentform',
                                'govtloanschemesform','pmcschemesform','utilitybillsform'
            ];

            for(var i=0;i<serviceforms.length;i++){
                manageCandidate.disableHelpingHandForm(serviceforms[i]);
            }
        }

        manageCandidate.disableHelpingHandForm = function(type){
            manageCandidate[type] = false;
            manageCandidate.showHHAddButton = true;
        }

        manageCandidate.HHAccordionClickHandler = function(category,dataArray,evt){
            manageCandidate.disableAllHelpingHandForms();
            if($(evt.target).parent().parent().attr('aria-expanded') == "false"){
                //manageCandidate.getAllServices( category, parseInt($stateParams.cid) , dataArray );  
                manageCandidate.getAllHelpingHandData( category, parseInt($stateParams.cid) , dataArray);  
            }
            
        }

        manageCandidate.goToeditHelpingHandCategory = function(serviceObj , formControl, formname , elem){
            manageCandidate[formControl] = angular.copy(serviceObj);
            manageCandidate.enableHelpingHandForm(formname);
            manageCandidate[formControl]['uploadtype'] = 'Url';
            $('html, body').animate({
                 scrollTop: $("#"+ elem).offset().top
            }, 500);
        }

        manageCandidate.clearHHFormData = function(form ,  formControl){
            form.$setPristine();
            form.$setUntouched();
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControl] = {'hhstatus':'1','uploadtype':'Url'};
        }

        manageCandidate.addHelpingHandCategory = function(form,category,formControl,listArray){
            try{
                manageCandidate.isButtonClicked = true;
                if(!form.$valid){
                    $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
                    return;
                }
                //return;
                utilityService.showWaitLoader();
                if(manageCandidate[formControl].hhdataid){ //Edit Mode
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_HELPING_HAND;
                }else{
                    var postUrl = CONSTANTS.BASE_URL +  CONSTANTS.ADD_NEW_HELPING_HAND;  //'temptest.php';//
                }
                
                manageCandidate[formControl]['CID'] = utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                manageCandidate[formControl]['hhmaincategory'] = category;
                
                //return;
                utilityService.requestFilePOSTHttpService(postUrl, manageCandidate[formControl] ,function(data){
                     console.log("inside addHelpingHandCategory success",data);
                     if(data.status == 'OK'){
                         utilityService.showAlertMessage(data.message);
                         utilityService.hideWaitLoader();
                         manageCandidate.clearHHFormData(form , formControl);
                         manageCandidate.disableHelpingHandForm(category.split(' ').join('').toLowerCase() + 'form');   
                         
                         
                          manageCandidate.getAllHelpingHandData( category, parseInt($stateParams.cid) , listArray);
                     }else{
                        utilityService.showAlertMessage(data.message);
                     }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });
            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.updateHelpingHandStatus = function(status,category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_HELPING_HAND;
                var postData = angular.copy(serviceObj);
                postData['CID'] = serviceObj.candidateid;
                postData['hhstatus'] = status; //((serviceObj.servicestatus == 1) ? 0 : 1);
               /* var postData = {
                    'servicecategoryid':service.servicecategoryid,
                    'CID':service.candidateid,
                    'servicestatus':((service.servicestatus == 1) ? 0 : 1)
                }*/
                utilityService.requestFilePOSTHttpService(postUrl, postData ,function(data){
                         if(data.status == 'OK'){
                            utilityService.hideWaitLoader();
                            manageCandidate.getAllHelpingHandData( category, parseInt($stateParams.cid) , listArray);
                        }else{
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage(data.message);    
                        }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.getAllHelpingHandData = function(category, CID , HHName ){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_HELPING_HAND_LIST;
                var postData = {'hhmaincategory':category , 'CID':CID };
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("HH fetched from server",data);
                        if(data.hhdata && data.hhdata.length > 0){
                            manageCandidate[HHName] = data.hhdata;    
                        }else{
                            manageCandidate[HHName] = [];
                        }
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                //utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.deleteHelpingHandCategory = function(category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.DELETE_HELPING_HAND;
                var postData = {'hhdataid':serviceObj.hhdataid};
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("HH Deleted",data);
                        manageCandidate.getAllHelpingHandData( category, parseInt($stateParams.cid) , listArray);
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }
        /*Helping Hand Category End*/

        /*Matdar Jagruti Start*/
        manageCandidate.enableMatdarJagrutiForm = function(type , formControlName){
            manageCandidate[type] = true;
            manageCandidate.showMatdarJagrutiAddButton = false;
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControlName] = {'mjstatus':'1','uploadtype':'Url'};
        }

        manageCandidate.disableAllMatdarJagrutiFormForms = function(){
            var serviceforms = ['matdarjagrutiform'
            ];

            for(var i=0;i<serviceforms.length;i++){
                manageCandidate.disableMatdarJagrutiForm(serviceforms[i]);
            }
        }

        manageCandidate.disableMatdarJagrutiForm = function(type){
            manageCandidate[type] = false;
            manageCandidate.showMatdarJagrutiAddButton = true;
        }

        manageCandidate.goToeditMatdarJagrutiCategory = function(serviceObj , formControl, formname , elem){
            manageCandidate[formControl] = angular.copy(serviceObj);
            manageCandidate.enableMatdarJagrutiForm(formname);
            manageCandidate[formControl]['uploadtype'] = 'Url';
            $('html, body').animate({
                 scrollTop: $("#"+ elem).offset().top
            }, 500);
        }

        manageCandidate.clearMatdarJagrutiFormData = function(form ,  formControl){
            form.$setPristine();
            form.$setUntouched();
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControl] = {'hhstatus':'1','uploadtype':'Url'};
        }

        manageCandidate.addMatdarJagrutiCategory = function(form,category,formControl,listArray){
            try{
                manageCandidate.isButtonClicked = true;
                if(!form.$valid){
                    $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
                    return;
                }
                //return;
                utilityService.showWaitLoader();
                if(manageCandidate[formControl].mjdataid){ //Edit Mode
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_MATDAR_JAGRUTI;
                }else{
                    var postUrl = CONSTANTS.BASE_URL +  CONSTANTS.ADD_MATDAR_JAGRUTI;  //'temptest.php';//
                }
                
                manageCandidate[formControl]['CID'] = utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                
                //return;
                utilityService.requestFilePOSTHttpService(postUrl, manageCandidate[formControl] ,function(data){
                     console.log("inside addMatdarJagrutiCategory success",data);
                     if(data.status == 'OK'){
                         utilityService.showAlertMessage(data.message);
                         utilityService.hideWaitLoader();
                         manageCandidate.clearMatdarJagrutiFormData(form , formControl);
                         manageCandidate.disableMatdarJagrutiForm(category.split(' ').join('').toLowerCase() + 'form');   
                         
                         
                         manageCandidate.getAllMatdarJagrutiData( category, parseInt($stateParams.cid) , listArray);
                     }else{
                        utilityService.showAlertMessage(data.message);
                     }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });
            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.updateMatdarJagrutiStatus = function(status,category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.UPDATE_MATDAR_JAGRUTI;
                var postData = angular.copy(serviceObj);
                postData['CID'] = serviceObj.candidateid;
                postData['mjstatus'] = status; //((serviceObj.servicestatus == 1) ? 0 : 1);
                utilityService.requestFilePOSTHttpService(postUrl, postData ,function(data){
                         if(data.status == 'OK'){
                            utilityService.hideWaitLoader();
                            manageCandidate.getAllMatdarJagrutiData( category, parseInt($stateParams.cid) , listArray);
                        }else{
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage(data.message);    
                        }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.getAllMatdarJagrutiData = function(category, CID , name ){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_MATDAR_JAGRUTI;
                var postData = {'CID':CID };
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("HH fetched from server",data);
                        if(data.mjdata && data.mjdata.length > 0){
                            manageCandidate[name] = data.mjdata;    
                        }else{
                            manageCandidate[name] = [];
                        }
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                //utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.deleteMatdarJagrutiCategory = function(category,serviceObj,listArray){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.DELETE_MATDAR_JAGRUTI;
                var postData = {'mjdataid':serviceObj.mjdataid};
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("Matadar Jagruti Deleted",data);
                        manageCandidate.getAllMatdarJagrutiData( category, parseInt($stateParams.cid) , listArray);
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }
        /*Matdar Jagruti Ends*/

        /*Matdar Jagruti Start*/
        manageCandidate.enableSocialNetForm = function(type , formControlName){
            manageCandidate[type] = true;
            manageCandidate.showSocailNetAddButton = false;
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControlName] = {'status':'1'};
        }

        manageCandidate.disableAllSocialNetForms = function(){
            var serviceforms = ['socialnetform'
            ];

            for(var i=0;i<serviceforms.length;i++){
                manageCandidate.disableSocialNetForm(serviceforms[i]);
            }
        }

        manageCandidate.disableSocialNetForm = function(type){
            manageCandidate[type] = false;
            manageCandidate.showSocailNetAddButton = true;
        }

        manageCandidate.goToeditSocialNetCategory = function(serviceObj , formControl, formname , elem){
            manageCandidate[formControl] = angular.copy(serviceObj);
            manageCandidate[formControl]['filedata'] = manageCandidate[formControl]['socialimage'];
            manageCandidate.enableSocialNetForm(formname);
            //manageCandidate[formControl]['uploadtype'] = 'Url';
            $('html, body').animate({
                 scrollTop: $("#"+ elem).offset().top
            }, 500);
        }

        manageCandidate.clearSocialNetFormData = function(form ,  formControl){
            form.$setPristine();
            form.$setUntouched();
            manageCandidate.isButtonClicked = false;
            manageCandidate[formControl] = {'status':'1'};
        }

        manageCandidate.addSocialNetCategory = function(form,category,formControl,listArray){
            
            try{
                manageCandidate.isButtonClicked = true;
                if(!form.$valid){
                    $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
                    return;
                }
                //return;
                utilityService.showWaitLoader();
                var action;
                if(manageCandidate[formControl].socialid){ //Edit Mode
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SOCIAL_URL;
                    action = 'updatesocialurl';
                }else{
                    var postUrl = CONSTANTS.BASE_URL +  CONSTANTS.SOCIAL_URL;  //'temptest.php';//
                    action = 'addsocialurl';
                }
                
                manageCandidate[formControl]['CID'] = utilityService.getDataFromLocalStorage('currentCreatedCandidate');
                manageCandidate[formControl]['action']  = action;
                
                
                //return;
                utilityService.requestPOSTHttpService(postUrl, manageCandidate[formControl] ,function(data){
                     console.log("inside addSocailNetCategory success",data);
                     if(data.status == 'OK'){
                         utilityService.showAlertMessage(data.message);
                         utilityService.hideWaitLoader();
                         manageCandidate.clearSocialNetFormData(form , formControl);
                         manageCandidate.disableSocialNetForm('socialnetform');   
                         
                         
                         manageCandidate.getAllSocialNetData( category, parseInt($stateParams.cid) , listArray);
                     }else{
                        utilityService.showAlertMessage(data.message);
                     }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });
            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.updateSocialNetStatus = function(status,category,serviceObj,listArray){
            
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SOCIAL_URL;
                var postData = angular.copy(serviceObj);
                postData['CID'] = serviceObj.candidateid;
                postData['status'] = status; 
                postData['action']  = 'updatesocialurl';
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                         if(data.status == 'OK'){
                            utilityService.hideWaitLoader();
                            manageCandidate.getAllSocialNetData( category, parseInt($stateParams.cid) , listArray);
                        }else{
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage(data.message);    
                        }
                },function(error){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");    
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.getAllSocialNetData = function(category, CID , HHName ){
            
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SOCIAL_URL_GET;
                var postData = {'action':'getsocialurl' , 'CID':CID };
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("HH fetched from server",data);
                        if(data.socialurl && data.socialurl.length > 0){
                            manageCandidate[HHName] = data.socialurl;    
                        }else{
                            manageCandidate[HHName] = [];
                        }
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                //utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        manageCandidate.deleteSocialNetCategory = function(category,serviceObj,listArray){
            
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.SOCIAL_URL_GET;
                var postData = {'socialid':serviceObj.socialid,'action':'deletesocialurl'};
                utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                    if(data.status == 'OK'){
                        //utilityService.showAlertMessage(data.message);
                        console.log("deleteSocialNetCategory Deleted",data);
                        manageCandidate.getAllSocialNetData( category, parseInt($stateParams.cid) , listArray);
                        
                    }else{
                        utilityService.showAlertMessage(data.message);
                    }
                    utilityService.hideWaitLoader();
                },function(err){
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });

            }catch(ex){
                console.log(ex);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }
        /*Social Net Ends*/

        /*Image Cropper Controller**/
        /*manageCandidate.cropImageHandlerCtrl = function($scope,$uibModalInstance,image,params){
                $scope.croppedImage='';
                $scope.image = image;
                $scope.params = params;

                $scope.doneCropping = function(croppedImage){
                    $uibModalInstance.close('');
                    if(params.formControlName == null){
                        //manageCandidate.$apply(function($scope){
                            manageCandidate[params.modelName] = croppedImage;
                        //});
                        
                    }else{
                        if(params.subFormControl){
                            manageCandidate[params.formControlName][params.subFormControl][params.modelName] = croppedImage;
                        }else{
                            manageCandidate[params.formControlName][params.modelName] = croppedImage;    
                        }
                    }
                    
                };

                $scope.unDoneCropping = function(){
                    $uibModalInstance.close('');
                    if(params.formControlName == null){
                        manageCandidate[params.modelName] = image;
                    }else{
                        if(params.subFormControl){
                            manageCandidate[params.formControlName][params.subFormControl][params.modelName] = image;
                        }else{
                            manageCandidate[params.formControlName][params.modelName] = image;    
                        }
                    }
                }
        }*/


        manageCandidate.handleFileSelect = function(file,formControlName,modelName,subFormControl){
            console.log("handleFileSelect callled",file);
                if(!file){
                    return;
                }
              var reader = new FileReader();
              reader.onload = function (evt) {
                utilityService.showPopUp({
                    url:'./app/shared/popups/imageCropperPopup.html',
                    ctrl: 'cropImageHandlerCtrl',//'modalPopupCtrl', //manageCandidate.cropImageHandlerCtrl,
                    image:evt.target.result,
                    params:{formControlName:formControlName , modelName:modelName , subFormControl:subFormControl , scopeObj:manageCandidate}
                });
              };
              reader.readAsDataURL(file);

        }

        


        
        /*---------------------------*/
          $scope.oneAtATime = true;

          $scope.groups = [
            {
              title: 'Dynamic Group Header - 1',
              content: 'Dynamic Group Body - 1'
            },
            {
              title: 'Dynamic Group Header - 2',
              content: 'Dynamic Group Body - 2'
            }
          ];

          $scope.items = ['Item 1', 'Item 2', 'Item 3'];

          $scope.addItem = function() {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
          };

          $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
          };
        /*---------------------------*/


    }]);


//})(socialApp.controllers);

//cropImageHandlerCtrl
angular.module('utilModule').controller('cropImageHandlerCtrl',['$scope','$uibModalInstance','$rootScope','image','params',
        function($scope, $uibModalInstance,$rootScope,image,params) {

            $scope.croppedImage='';
                $scope.image = image;
                $scope.params = params;

                $scope.doneCropping = function(croppedImage){
                    $uibModalInstance.close('');
                    if(params.formControlName == null){
                        //manageCandidate.$apply(function($scope){
                            params.scopeObj[params.modelName] = croppedImage;
                        //});
                        
                    }else{
                        if(params.subFormControl){
                            params.scopeObj[params.formControlName][params.subFormControl][params.modelName] = croppedImage;
                        }else{
                            params.scopeObj[params.formControlName][params.modelName] = croppedImage;    
                        }
                    }
                    
                };

                $scope.unDoneCropping = function(){
                    $uibModalInstance.close('');
                    if(params.formControlName == null){
                        params.scopeObj[params.modelName] = params.scopeObj[params.modelName];
                    }else{
                        if(params.subFormControl){
                            params.scopeObj[params.formControlName][params.subFormControl][params.modelName] = params.scopeObj[params.formControlName][params.subFormControl][params.modelName];
                        }else{
                            params.scopeObj[params.formControlName][params.modelName] = params.scopeObj[params.formControlName][params.modelName];    
                        }
                    }
                };

                $scope.doNotCropping = function(){
                    $uibModalInstance.close('');
                    if(params.formControlName == null){
                        params.scopeObj[params.modelName] = image;
                    }else{
                        if(params.subFormControl){
                            params.scopeObj[params.formControlName][params.subFormControl][params.modelName] = image;
                        }else{
                            params.scopeObj[params.formControlName][params.modelName] = image;
                        }
                    }
                }


}]);