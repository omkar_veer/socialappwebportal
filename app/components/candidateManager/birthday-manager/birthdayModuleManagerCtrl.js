//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('birthdayModuleManagerCtrl',['$scope','$rootScope','utilityService','$state', '$compile', '$timeout', 'uiCalendarConfig',
    	function($scope,$rootScope,utilityService,$state,$compile, $timeout, uiCalendarConfig) {
         console.log("inside birthdayModuleManagerCtrl");

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.events = [];
    $scope.a_events = [];

    $scope.birthDaysEventsObj = {
       color: '#286090',
       textColor: '#fff',
       events: [
          
        ]
    };

    $scope.anniEventsEventsObj = {
       color: '#f00',
       textColor: 'yellow',
       events: [
          
        ]
    };




    //;
    setTimeout(function(){
        $('.fc-corner-right , .fc-corner-left').on('click',function(){
          $scope.getBirthDays();
        });
    },3000);

    $scope.prepareDataForCalender = function(birthdays, anniversaries){
        var tempArr = [];
        //birthdays.push({"firstname":"Aniket","middlename":"C","lastname":"Karmarkar","email":"aniketjack@gmail.com","phone":"9762414243","dob":"1988-04-03"});
        //anniversaries.push({"firstname":"Aniket","middlename":"C","lastname":"Karmarkar","email":"aniketjack@gmail.com","phone":"9762414243","anniversary":"2005-04-08"});
        for(var i=0;i<birthdays.length;i++){
            var iteratorDate = new Date(birthdays[i].dob);
            var iteratorM  = iteratorDate.getMonth();
            var iteratorD  = iteratorDate.getDate();
           
            if(!birthdays[i].ignore){
              var filterBirthdays = _.filter(birthdays, function(obj){ 
                  var date = new Date(obj.dob);
                  var m  = date.getMonth();
                  var d  = date.getDate(); 
                  return (iteratorM == m && iteratorD == d);
                         
              });
              console.log("filterBirthdays:::::::",filterBirthdays);
              if(filterBirthdays.length > 0){
                  var tempDate = new Date(filterBirthdays[0].dob);
                  var tempMonth  = tempDate.getMonth();
                  var tempDay  = tempDate.getDate();
                  if(filterBirthdays.length <= 1){
                    $scope.birthDaysEventsObj.events.push({'type':'birthdays','date':filterBirthdays[0].dob,'title':filterBirthdays.length + ' Birthday',start:new Date(y, tempMonth, tempDay)});
                  }else{
                     $scope.birthDaysEventsObj.events.push({'type':'birthdays','date':filterBirthdays[0].dob,'title':filterBirthdays.length + ' Birthdays',start:new Date(y, tempMonth, tempDay)});
                  }

                  for(var j=0;j<filterBirthdays.length;j++){
                      filterBirthdays[j]['ignore'] = true;
                  }
                  
              }
            }
            

        }

        //anniversaries
        for(var i=0;i<anniversaries.length;i++){
            var iteratorDate = new Date(anniversaries[i].anniversary);
            var iteratorM  = iteratorDate.getMonth();
            var iteratorD  = iteratorDate.getDate();
             if(!anniversaries[i].ignore){
                var filteranniversaries = _.filter(anniversaries, function(obj){ 
                    var date = new Date(obj.anniversary);
                    var m  = date.getMonth();
                    var d  = date.getDate();     
                    return (iteratorM == m && iteratorD == d);
                           
                });
                console.log("filteranniversaries:::::::",filteranniversaries);
                if(filteranniversaries.length > 0){
                    var tempDate = new Date(filteranniversaries[0].anniversary);
                    var tempMonth  = tempDate.getMonth();
                    var tempDay  = tempDate.getDate();
                     if(filteranniversaries.length <= 1){
                        $scope.anniEventsEventsObj.events.push({'type':'anniversaries','date':filteranniversaries[0].anniversary,'title':filteranniversaries.length + ' Anniversary',start:new Date(y, tempMonth, tempDay)});
                      }else{
                        $scope.anniEventsEventsObj.events.push({'type':'anniversaries','date':filteranniversaries[0].anniversary,'title':filteranniversaries.length + ' Anniversaries',start:new Date(y, tempMonth, tempDay)});
                      }

                      for(var j=0;j<filteranniversaries.length;j++){
                          filteranniversaries[j]['ignore'] = true;
                      }
                }
              }

        }

        console.log("Finally",$scope.events,$scope.a_events);
        
    }

    $scope.getBirthDays = function () {
           try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.BIRTHDAY_LIST;
                    var postData = {
                                    "CID" :  getParameterByName('cid') || utilityService.getDataFromLocalStorage('CID'),
                                    
                                };
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside getBirthDays success',data);
                        utilityService.hideWaitLoader();
                        if(data.status == 'OK'){
                            $scope.birthdays = data.birthdays || [];
                            $scope.anniversaries = data.anniversaries || [];
                            //if(data && $scope.birthdays.length > 0){
                              $scope.prepareDataForCalender($scope.birthdays, $scope.anniversaries);  
                            //}
                            
                        }else{
                            utilityService.showAlertMessage("Something went wrong please try again");

                        }

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        };

    $scope.getBirthDays();

    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };
    /* event source that contains custom events on the scope */
    /*$scope.events = [
      {title: 'Birthday',start: new Date("2016-04-18")},
      {title: 'Birthday',start: new Date(y, m, 2)},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
      {title: 'Birthday',start: new Date(y, m, 12)}
      {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ];*/
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( data, jsEvent, view){
    	console.log("clicked",data);
      var monthHeader = '';
      if(data.type == 'birthdays'){
          var filterData = _.filter($scope.birthdays, function(obj){ 
                    var date = new Date(obj.dob);
                    var m  = date.getMonth();
                    var d  = date.getDate(); 
                    var compDate = new Date(data.date);    
                    var compMonth  = compDate.getMonth();
                    var compDay  = compDate.getDate(); 
                    return (compMonth == m && compDay == d);
                           
          });
          if(filterData.length > 0){
            var tempDate = new Date(filterData[0].dob),
            tempDay = tempDate.getDate() , tempMonth = tempDate.getMonth();
            monthHeader = tempDay +' '+ moment.months()[tempMonth];
          }

      }else{
          var filterData = _.filter($scope.anniversaries, function(obj){ 
                    var date = new Date(obj.anniversary);
                    var m  = date.getMonth();
                    var d  = date.getDate();     
                    monthHeader = d +' '+ moment.months()[m];
                    var compDate = new Date(data.date);    
                    var compMonth  = compDate.getMonth();
                    var compDay  = compDate.getDate(); 
                    return (compMonth == m && compDay == d);
                           
          });

          if(filterData.length > 0){
            var tempDate = new Date(filterData[0].anniversary),
            tempDay = tempDate.getDate() , tempMonth = tempDate.getMonth();
            monthHeader = tempDay +' '+ moment.months()[tempMonth];
          }
      }
      console.log("filterData::",filterData);
      if(filterData.length > 0){
        utilityService.showBirthdayListPopup({
            type:data.type,
            list:filterData,
            month: monthHeader

        });  
      }
      
      
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
    /* add custom event*/
    $scope.addEvent = function() {
      $scope.events.push({
        title: 'Open Sesame',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        className: ['openSesame']
      });
    };
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.changeView = function(view,calendar) {
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      $timeout(function() {
        if(uiCalendarConfig.calendars[calendar]){
          uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
      });
    };
     /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) {
        element.attr({'tooltip': event.title,
                      'tooltip-append-to-body': true});
        $compile(element)($scope);
    };

    //$scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: false,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender,
        dayNamesShort:["रविवार", "सोमवार", "मंगळवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार"],
        monthNames:["जानेवारी","फेब्रुवारी","मार्च","एप्रिल","मे","जून","जुलै","ऑगस्ट","सप्टेंबर","ऑक्टोबर","नोव्हेंबर","डिसेंबर"]
      }
    };

    $scope.changeLang = function() {
      if($scope.changeTo === 'Hungarian'){
        $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
        $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
        $scope.changeTo= 'English';
      } else {
        $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        $scope.changeTo = 'Hungarian';
      }
    };
    /* event sources array*/
    $scope.eventSources = [$scope.birthDaysEventsObj, $scope.eventSource, $scope.eventsF,$scope.anniEventsEventsObj];
    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];


    $scope.sendWishes = function(type){
        try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.BULK_SMS_SERVICE;
                    var postData = {
                                      "CID" :  getParameterByName('cid') || utilityService.getDataFromLocalStorage('CID'),
                                      "occasion":type     
                                   };
                    utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                        console.log('inside sendWishes success',data);
                        utilityService.hideWaitLoader();
                        //if(data.status == 'OK'){
                        utilityService.showAlertMessage("Wishes sent successfully !!!");
                        //}else{
                            //utilityService.showAlertMessage("Something went wrong please try again");

                        //}

                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }

    };

        

       

    }]);


//})(socialApp.controllers);