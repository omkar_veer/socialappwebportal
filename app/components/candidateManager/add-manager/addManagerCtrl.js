//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('addManagerCtrl',['$scope','$rootScope','utilityService','$state', 
    	function($scope,$rootScope,utilityService,$state) {
         console.log("inside addManagerCtrl");

        $scope.karyakartaFormControl = {};
        $scope.karyakartaFormControl.profile = {
        	'isVoter':1,
        	'role':'manager',
        	'ischairman':0

        };
        addGlobalEventHandler();
        $scope.professionList = CONSTANTS.HARDCODED_VALUES.PROFESSION_LIST;
        $scope.educationList = CONSTANTS.HARDCODED_VALUES.EDUCATION_LIST;
        $scope.bloodgrpList = CONSTANTS.HARDCODED_VALUES.BLOODGRP_LIST;
        

         $scope.OTPForm = {};

        var date = new Date()
        date.setMonth(date.getMonth() - 216);
        $scope.maxDateLimit = date;
        $scope.minDateLimit = new Date('1900/1/1');
        $scope.dateFormat = 'yyyy-MM-dd';
        //$scope.dateFormat = 'dd/MM/yyyy';

        $scope.getCastList = function(){
            utilityService.getCastList(function(data){
                $scope.casteList = data;
            },function(){

            });
        }

        $scope.getAllStates = function(){
            utilityService.getAllStates(function(data){
                $scope.states = data;
            },function(){

            });

        }

        $scope.getDistricts = function(state){
            utilityService.getDistricts( state , function(data){
                $scope.districts = data;
            },function(){

            });


        }

        $scope.getTalukas = function(district){
            utilityService.getTalukas(district,function(data){
                $scope.talukas = data;
            },function(){

            });

        }


        $scope.getPincodes = function(taluka){
            utilityService.getPincodes(taluka,function(data){
                $scope.pincodes = data;
            },function(){

            });

        }

        $scope.getReligionList=  function(){
            utilityService.getReligionList(function(data){
                $scope.religionList = data;
            },function(){

            });
        }

        $scope.$on('OTPVerified', function(event,data){
            if(window.location.hash.indexOf('utility') != -1){
                $scope.karyakartaFormControl = {};
                $scope.karyakartaFormControl.profile = {
                    'isVoter':1,
                    'role':'manager',
                    'ischairman':0

                };
                 $scope.addmanagerform.$setPristine();
                 $scope.addmanagerform.$setUntouched();
                 $scope.isButtonClicked = false;
            }else{
                $state.transitionTo('candidatedashboard',{}, CONSTANTS.STATE_TRANS_OPTIONS);    
            }
            
        });

        $scope.getShareLink = function(candidate){
                var managerLoginlink = window.location.origin + window.location.pathname +'karyakarta/#/login?cid='+ utilityService.getDataFromLocalStorage('CID');
                var modalInstance = utilityService.showInputAlertMessage({
                        'managerLogin':managerLoginlink
                });
                   

                modalInstance.result.then(function () {
                  
                }, function () {
                    console.log("cancled");  
                });
        }

        $scope.createManagerProfile = function(addmanagerform){
            try{
                    console.log("inside candidate profile save",$scope.karyakartaFormControl);
                    $scope.isButtonClicked = true;
                   
                    if(!addmanagerform.$valid){
                        $("[name='" + addmanagerform.$name + "']").find('.ng-invalid:visible:first').focus();
                        return;
                    }
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.CITIZEN_REGISTRATION;
                    
                    $scope.karyakartaFormControl.profile['CID'] = utilityService.getDataFromLocalStorage('CID');
                    $scope.karyakartaFormControl.profile['registeredFrom'] = 'Portal';
                    console.log('Before save',$scope.karyakartaFormControl.profile);
                    //return;
                    utilityService.requestPOSTHttpService(postUrl, $scope.karyakartaFormControl.profile ,function(data){
                        console.log('inside create candidate success',data);
                        //$scope.locationMasterData = data;
                        utilityService.hideWaitLoader();
                        $scope.isButtonClicked = false;
                            if(typeof data == 'string'){
                            	var splitedStr = data.split('{') //[1];
	                            if(splitedStr.length != 0){
	                                    //splitedStr[1];
	                                    var strRes = JSON.parse(splitedStr[1].insert(0, "{"));

	                                    if(strRes.status == 'OK'){
	                                        if(strRes.CTID){
	                                            $scope.OTPForm.phoneNumber = $scope.karyakartaFormControl.profile.phone;
	                                            $scope.OTPForm.CTID = strRes.CTID;
	                                            $scope.OTPForm.type = 'karyakarta';
	                                            utilityService.showOTPAlertMessage($scope);
	                                            
	                                        }

	                                    }else{
	                                        utilityService.showAlertMessage(strRes.message);

	                                    }

	                            }else{
	                                utilityService.showAlertMessage("Something went wrong please try again");
	                            }
                            }else{
                            	if(data.status == 'FAILED'){
                            		/*if(data.otp == 'pending'){

                            		}else{*/
                            			utilityService.showAlertMessage(data.message);	
                            		//}
                            		

                            	}
                            }
                            
                        
                        
                    },function(err){
                        console.log('inside create manager error',err);
                        utilityService.hideWaitLoader();
                        utilityService.showAlertMessage("Something went wrong please try again");
                       
                    });
            

            }catch(e){
                     utilityService.hideWaitLoader();
            }
        }

        



        

       

    }]);


//})(socialApp.controllers);