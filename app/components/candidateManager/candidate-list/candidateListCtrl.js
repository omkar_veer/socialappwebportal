//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('candidateListCtrl',['$scope','$rootScope','utilityService','$state','isAuthorised', 
        function($scope,$rootScope,utilityService,$state,isAuthorised) {
        console.log("inside candidateListCtrl");

        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.searchControl ={};
        $scope.filterControl = {};
        $scope.searchControl.searchMode = 'district';
        $scope.searchControl.state = 'MAHARASHTRA';

        $scope.sortReverse = false;
        $scope.sortType = 'firstname';
        $scope.isSearchedClicked = false;
         $scope.showFilterPanel = false;
        $scope.OTPForm = {};
        



        var date = new Date()
        date.setMonth(date.getMonth() - 216);
        $scope.maxDateLimit = date;
        $scope.minDateLimit = new Date('1900/1/1');

        showGlobalMenu();
        utilityService.deleteSessionStorageItem("currentCreatedCandidate");

        $scope.AddNewCandidate = function(){
        	$state.transitionTo('createcandidate',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        $scope.$watch('candidateList', function(value) {
            $scope.constructExportList();
                
        });

        $scope.$watch('searchControl.searchMode', function(value) {
            $scope.filterControl.firstname = $scope.filterControl.lastname = $scope.filterControl.middlename = "";
            $scope.searchControl.firstname = $scope.searchControl.lastname = $scope.searchControl.middlename = "";
            $scope.searchControl.district = $scope.searchControl.vidhansabha = $scope.searchControl.localbody = $scope.searchControl.loksabha = null;
            $scope.showFilterPanel = false;
        });

        

        $scope.constructExportList = function(){
            $scope.exportCandidateList = _.map($scope.candidateList, function(o) { return _.omit(o, 'image') && _.omit(o, 'verified'); });     
        }


        $scope.getShareLink = function(candidate){
            var citizenRegilink = window.location.origin + window.location.pathname +'citizen/#/main?cid='+ candidate.CID;
            var managerLoginlink = window.location.origin + window.location.pathname +'karyakarta/#/login?cid='+ candidate.CID;
            if(candidate.subscription == 'Birthday'){
                var modalInstance = utilityService.showInputAlertMessage({
                    'managerLogin':managerLoginlink
                });
            }else{
                var modalInstance = utilityService.showInputAlertMessage({
                    'citizenRegi':citizenRegilink,
                    'managerLogin':managerLoginlink
                });    
            }
            
               

            modalInstance.result.then(function () {
              
            }, function () {
                console.log("cancled");  
            });
        }

        

        
        /*Candidate List Start Here**/
        $scope.getCandidateList = function (){
            try{
                utilityService.showWaitLoader();
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_CANDIDATE;
                    
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getCandidateList success',data);
                    
                    if(data.status == 'OK' && data.candidate){
                            $scope.candidateList = data.candidate;
                            $scope.OriginalList = data.candidate;
                    }else if(data.status == 'OK' && !data.candidate){
                        utilityService.showAlertMessage('There are no candidate,You can add New candidate by clicking on Add New Candidate button');
                           
                    }else if(data.status == 'FAILED'){
                            utilityService.showAlertMessage(data.message);
                    }
                    
                    utilityService.hideWaitLoader();
                },function(err){
                    console.log('inside getCandidateList error',err);
                    //alert("Something went wrong please try again");
                    utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });
            }catch(e){
                console.log('inside getCandidateList error',e);
                utilityService.hideWaitLoader();
                utilityService.showAlertMessage("Something went wrong please try again");
            }   
        }

        

        $scope.getHeader = function () {
            var arr = [];
            for(var obj in $scope.exportCandidateList[0]){
                if(obj != 'verified'){
                    arr.push(obj);
                }
                
            }
            return arr;
        };


        $scope.removeSearchFilter = function(){
                $scope.candidateList = $scope.OriginalList;
                $scope.searchControl ={};
                $scope.filterControl ={};
                $scope.showFilterPanel = false;
                $scope.searchControl.searchMode = 'district';
                $scope.searchControl.state = 'MAHARASHTRA';
        }

        $scope.searchByName = function(){
                if( ($scope.searchControl.firstname &&  $scope.searchControl.firstname != "" )
                    && ($scope.searchControl.middlename &&  $scope.searchControl.middlename != "" )
                    && ($scope.searchControl.lastname &&  $scope.searchControl.lastname != "" )){
                    $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.searchControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.searchControl.middlename).trim().toLowerCase()) == 0)
                           && ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.searchControl.lastname).trim().toLowerCase()) == 0)
                    });

                }


                else if( ($scope.searchControl.firstname &&  $scope.searchControl.firstname != "" )
                    && ($scope.searchControl.middlename &&  $scope.searchControl.middlename != '' )){
                    $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.searchControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.searchControl.middlename).trim().toLowerCase()) == 0);
                           
                    });

                }

                else if(($scope.searchControl.firstname &&  $scope.searchControl.firstname != "" )
                    && ($scope.searchControl.lastname &&  $scope.searchControl.lastname != '' )){
                    $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.searchControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.searchControl.lastname).trim().toLowerCase()) == 0)
                    });

                }

                else if (($scope.searchControl.firstname &&  $scope.searchControl.firstname != "" ) ){
                    $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.searchControl.firstname).trim().toLowerCase()) != -1) 
                           
                    });

                }

                else if( ($scope.searchControl.lastname &&  $scope.searchControl.lastname != "" )
                    && ($scope.searchControl.middlename &&  $scope.searchControl.middlename != '' )){
                    $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.searchControl.lastname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.searchControl.middlename).trim().toLowerCase()) == 0);
                           
                    });

                }


        }

        $scope.searchByState = function(){
                $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return (obj['state'] ==  $scope.searchControl.state ); 
                       
                });
                if($scope.candidateList.length > 0){
                    $scope.showFilterPanel = true;
                }
        }

        $scope.searchByDistrict = function(){

            $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    
                    return (obj['district'] ==  $scope.searchControl.district ); 
                       
            });
            if($scope.candidateList.length > 0){
                    $scope.showFilterPanel = true;
            }
            
        }

        $scope.searchByLoksabha = function (){
                
                $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    if($scope.searchControl.loksabha)
                    return ((obj['loksabha'].trim().toLowerCase()).indexOf(($scope.searchControl.loksabha).trim().toLowerCase()) != -1);
                });
                if($scope.candidateList.length > 0){
                    $scope.showFilterPanel = true;
                }
        }

        $scope.searchByVidhansabha = function (){
            $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                if($scope.searchControl.vidhansabha)
                return ((obj['vidhansabha'].trim().toLowerCase()).indexOf(($scope.searchControl.vidhansabha).trim().toLowerCase()) != -1);
            });
            if($scope.candidateList.length > 0){
                    $scope.showFilterPanel = true;
            }
            
        }

        $scope.searchByLocalBody = function (){
            $scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    if($scope.searchControl.localbody)
                    return ((obj['ward'].trim().toLowerCase()).indexOf(($scope.searchControl.localbody).trim().toLowerCase()) != -1);
            });
            if($scope.candidateList.length > 0){
                    $scope.showFilterPanel = true;
            }
            
        }

        $scope.removeFilter = function(){
            $scope.searchHandler();
            $scope.filterControl ={};
        }

        $scope.filterHandler = function(filterform){
            try{
                if( ($scope.filterControl.firstname &&  $scope.filterControl.firstname != "" )
                    && ($scope.filterControl.middlename &&  $scope.filterControl.middlename != "" )
                    && ($scope.filterControl.lastname &&  $scope.filterControl.lastname != "" )){
                    $scope.candidateList = _.filter($scope.candidateList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.filterControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.filterControl.middlename).trim().toLowerCase()) == 0)
                           && ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.filterControl.lastname).trim().toLowerCase()) == 0)
                    });

                }


                else if( ($scope.filterControl.firstname &&  $scope.filterControl.firstname != "" )
                    && ($scope.filterControl.middlename &&  $scope.filterControl.middlename != '' )){
                    $scope.candidateList = _.filter($scope.candidateList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.filterControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.filterControl.middlename).trim().toLowerCase()) == 0);
                           
                    });

                }

                else if(($scope.filterControl.firstname &&  $scope.filterControl.firstname != "" )
                    && ($scope.filterControl.lastname &&  $scope.filterControl.lastname != '' )){
                    $scope.candidateList = _.filter($scope.candidateList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.filterControl.firstname).trim().toLowerCase()) == 0) 
                           && ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.filterControl.lastname).trim().toLowerCase()) == 0)
                    });

                }

                else if (($scope.filterControl.firstname &&  $scope.filterControl.firstname != "" ) ){
                    $scope.candidateList = _.filter($scope.candidateList, function(obj){ 
                    
                    return ((obj['firstname'].trim().toLowerCase()).indexOf(($scope.filterControl.firstname).trim().toLowerCase()) != -1) 
                           
                    });

                }

                else if( ($scope.filterControl.lastname &&  $scope.filterControl.lastname != "" )
                    && ($scope.filterControl.middlename &&  $scope.filterControl.middlename != '' )){
                    $scope.candidateList = _.filter($scope.candidateList, function(obj){ 
                    
                    return ((obj['lastname'].trim().toLowerCase()).indexOf(($scope.filterControl.lastname).trim().toLowerCase()) == 0) 
                           && ((obj['middlename'].trim().toLowerCase()).indexOf(($scope.filterControl.middlename).trim().toLowerCase()) == 0);
                           
                    });

                }
            }catch(e){

            }
        }

        $scope.searchHandler = function(searchform){
            try{
                
                /*if(!searchform.$valid){
                    return;
                }*/
                $scope.isSearchedClicked = true;
                
                switch($scope.searchControl.searchMode){
                    case 'name': $scope.searchByName();
                                break;
                    
                    case 'state': $scope.searchByState();
                                 break;
                    case 'district':$scope.searchByDistrict();
                                 break;
                    case 'loksabha':$scope.searchByLoksabha();
                                break;
                    case 'vidhansabha':$scope.searchByVidhansabha();
                                break;
                    case 'localbody':$scope.searchByLocalBody();
                                break;                        
                                              


                }

                /*$scope.candidateList = _.filter($scope.OriginalList, function(obj){ 
                    if(obj[$scope.searchControl.searchMode])
                    return ((obj[$scope.searchControl.searchMode].trim().toLowerCase()).indexOf(($scope.searchControl.searchKey).trim().toLowerCase()) != -1);
                });*/

            }catch(e){

            }

        }

        $scope.gotoCandidateProfile = function(candidate){
            $state.transitionTo('viewcandidate',{'cid':candidate.CID}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        $scope.goToeditCandidate = function(candidate){
            $state.transitionTo('editcandidate',{'cid':candidate.CID}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        $scope.gotoComplaints = function(candidate){
                $state.transitionTo('complaints',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        $scope.deleteCandidate = function(candidate){
                var modalInstance = utilityService.showConfirmationAlertMessage("Do you want to delete this candidate '" + candidate.firstname + "'?");

                modalInstance.result.then(function () {
                  console.log("Yes Delete It");
                  //TODO Server call

                   $scope.candidateList =  _.reject($scope.candidateList, function(d){ return d.CID === candidate.CID; });
                   $scope.OriginalList = _.reject($scope.OriginalList, function(d){ return d.CID === candidate.CID; });

                   var postUrl = CONSTANTS.BASE_URL + CONSTANTS.DELETE_CANDIDATE;
                   var postParam = {"CID":candidate.CID};
                    
                    utilityService.requestPOSTHttpService(postUrl, postParam ,function(data){
                        console.log('inside delete success',data);
                    },function(err){
                       
                    });
                    


                }, function () {
                    console.log("cancled");  
                });
                
        }


        $scope.getAllStates = function(){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_ALL_STATES;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.states){
                        $scope.states = data.states;
                        $scope.searchControl.state = 'MAHARASHTRA';
                        $scope.getDistricts($scope.searchControl.state);
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){

            }
            //$scope.states = ["Maharashtra"];

        }

        $scope.getDistricts = function(state){
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_DISTRICTS;
                var postParam =  {"state":state};
                utilityService.requestPOSTHttpService(postUrl,postParam,function(data){
                    console.log('inside getPoliticalPartyList success',data);
                    
                    if(data.status == 'OK' && data.districts){
                        $scope.districts = data.districts;
                    }else{
                        console.log('inside getPoliticalPartyList error',data);
                    }
                    
                },function(err){
                    console.log('inside getPoliticalPartyList error',err);
                    
                });
            }catch(e){
                    console.log(e);
            }
            //$scope.districts = ["Pune"];

        }


        $scope.verifyCandidateRegi = function (candidate) {
            try{
                    utilityService.showWaitLoader();
                    var postUrl = CONSTANTS.BASE_URL + CONSTANTS.RESEND_OTP;
                    var postData = {
                                    "phone" : candidate.phone
                                   };
                    utilityService.requestPOSTHttpService(postUrl, postData  ,function(data){
                        console.log('inside ResendOTP success',data);
                        utilityService.hideWaitLoader(); 
                        $scope.OTPForm.phoneNumber = candidate.phone;
                        $scope.OTPForm.CID = candidate.CID;
                        $scope.OTPForm.type = 'candidate';
                        utilityService.showOTPAlertMessage($scope);
                        
                    },function(err){
                        console.log('inside savePartyDetails error',err);
                        utilityService.hideWaitLoader(); 
                       utilityService.showAlertMessage("Something went wrong please try again");
                        
                    });
            

            }catch(e){
                utilityService.hideWaitLoader(); 
               utilityService.showAlertMessage("Something went wrong please try again");
            }
        };


        $scope.$on('OTPVerified', function(event,data){
            
            var match = _.find($scope.candidateList, function(item) { return item.CID === data.CID });
            if (match) {
                match.verified = '1';
            }

            var matchOriginalList = _.find($scope.OriginalList, function(item) { return item.CID === data.CID });
            if (matchOriginalList) {
                matchOriginalList.verified = '1';
            }

        });



        /*Candidate List Ends**/

       // setTimeout(function(){
            //$scope.getCandidateList();
        //},1000); 

        

        
            
        
        
        
        


    }]);


//})(socialApp.controllers);