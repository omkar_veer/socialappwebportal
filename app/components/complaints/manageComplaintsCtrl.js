//;(function (module, undefined) {
    'use strict';

    angular.module('socialApp').controller('manageComplaintsCtrl',['$scope','$rootScope','utilityService','isAuthorised','$state', '$stateParams',
    	function($scope,$rootScope,utilityService,isAuthorised,$state,$stateParams) {
        console.log("inside manageComplaintsCtrl");

        if(!isAuthorised){
            $state.transitionTo('login',{}, CONSTANTS.STATE_TRANS_OPTIONS);
        }
        addGlobalEventHandler();
        var CL = $scope;

        CL.itemsPerPage = 10;
        CL.currentPage = 1;

        CL.resolveFormControl = {};
        CL.isButtonClicked = false;
        CL.complaintListFilterFormControl = {};
        CL.isSearchedClicked = false;
        CL.complainStatisticsList = [];
        CL.filterCompleted  = true;  

        CL.complaintListFilterFormControl.filterBy = "";
        CL.complaintListFilterFormControl.complaintcategory = "";

        

        $(document).ready(function(){
            (function() {

                  [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                  });

            })();

        });

        CL.$watch('filterCompleted',function(val){
            if(val){
                setTimeout(function(){
                    $( "#todatepicker , #fromdatepicker" ).datepicker();
                    $( "#todatepicker , #fromdatepicker" ).datepicker("option", "dateFormat", "yy-mm-dd" );
                    var fromDt = new Date(); 
                    fromDt.setDate(fromDt.getDate() - 30);
                    var toDt = new Date();
                    toDt.setDate(toDt.getDate() + 30);
                    $( "#fromdatepicker" ).datepicker('setDate', fromDt);
                    $( "#todatepicker" ).datepicker('setDate', toDt);
                    $( "#todatepicker , #fromdatepicker" ).datepicker( "option", "minDate", new Date(2016, 1 - 1, 1) );
                    $("#fromdatepicker").datepicker("option","onClose",function(selectedDate){
                        $( "#todatepicker" ).datepicker( "option", "minDate", selectedDate );
                    })
                },2000);
                
            }
        });

        CL.getComplaintCat = function(successCB,errorCB){
            
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_COMPLAINT_CATEGORY;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getComplaintCategories success',data);
                    if(data.status == 'OK' && data.complaintcategory){
                        return successCB(data.complaintcategory);
                    }else{
                        console.log('inside getCastList error',data);
                        return errorCB();
                    }
                    
                },function(err){
                    console.log('inside getComplaintCategories error',err);
                    return errorCB();
                    
                });
            }catch(e){
                console.log('inside getComplaintCategories error',e);
                return errorCB();
            }
        }

        

        CL.getComplaintStatistics = function(){
            utilityService.showWaitLoader();
            try{
                //
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.COMPLAINT_STATISTICS;
                var postData = {'CID':utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate'),'complaintsfor':'candidate'};
                utilityService.requestPOSTHttpService(postUrl,postData,function(data){
                    if(data.status == 'OK' && data.complaintstatistics){
                        console.log("data.complaintstatistics===",data.complaintstatistics);
                        //data.complaintstatistics.push({"count":"4","complaintcategory":"Water Complaint","complaintstatus":"0"});
                        CL.getComplaintCat(function(res){
                            utilityService.hideWaitLoader();
                            console.log("data.categories===",res);
                            var tempArray = [];
                            for(var t=0;t<res.length;t++){
                                var tempObj = {};
                                var tempList = _.filter(data.complaintstatistics, function(obj){ 
                                    return (obj.complaintcategory == res[t].complaintcategory ); 
                                }); 
                                
                                if(tempList.length != 0){
                                   
                                   var pendingCount = 0,resolvedCount = 0; 
                                   for(var i=0;i<tempList.length;i++){
                                        if(tempList[i].complaintstatus == "1"){ //Pending is present
                                            pendingCount = pendingCount + parseInt(tempList[i].count);
                                            resolvedCount = resolvedCount + 0;
                                        }else if(tempList[i].complaintstatus == "0"){   //Resolved is present
                                            pendingCount = pendingCount + 0;
                                            resolvedCount = resolvedCount + parseInt(tempList[i].count);
                                        }
                                   }
                                   tempObj['complaintcategory'] = tempList[0].complaintcategory;
                                   tempObj['pendingCount'] = pendingCount;
                                   tempObj['resolvedCount'] = resolvedCount;
                                   tempArray.push(tempObj);
                                }else{
                                   tempObj['complaintcategory'] = res[t].complaintcategory;
                                   tempObj['pendingCount'] = 0;
                                   tempObj['resolvedCount'] = 0;
                                   tempArray.push(tempObj);
                                }
                            }

                            console.log("$$$$$$$$$$$$tempArray===",tempArray);
                            CL.complainStatisticsList = tempArray;
                            var totalPending = 0 , totalResolved = 0;
                            for(var j=0;j<tempArray.length;j++){
                                totalPending = parseInt(tempArray[j].pendingCount) + totalPending;
                                totalResolved = parseInt(tempArray[j].resolvedCount) + totalResolved;
                            }
                            console.log("$$$$$$$$$$$$tempArray===",totalPending,totalResolved);
                            CL.totalComplaintsCnt = {"totalPending":totalPending,"totalResolved":totalResolved};
                        },function(){
                            utilityService.hideWaitLoader();
                            utilityService.showAlertMessage(data.message);
                        });




                    }else{
                        utilityService.hideWaitLoader();
                        console.log('inside getComplaintStatistics error',data);
                        utilityService.showAlertMessage(data.message);
                    }
                    
                },function(err){
                     utilityService.hideWaitLoader();
                    utilityService.showAlertMessage("Something went wrong please try again");
                });
   
            }catch(e){
                 utilityService.hideWaitLoader();
                console.log('inside getComplaintStatistics error',e);
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }
        

        CL.getComplaints = function(){
          utilityService.showWaitLoader();
            try{
               
                var postData = {'CID':utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate'),'complaintsfor':'candidate'};
                utilityService.getComplaints(postData,function(data){
                	if(data.status == 'OK' && data.complaintlist){
                        $scope.myComplaintList = data.complaintlist;
                        $scope.OriginalComplaintList = data.complaintlist;
                        CL.exportComplaintList = data.complaintlist;
                    }else{
                        console.log('inside getCastList error',data);
                        utilityService.showAlertMessage(data.message);
                    }
                     utilityService.hideWaitLoader();
                },function(err){
                	 utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
                });
   
            }catch(e){
            	 utilityService.hideWaitLoader();
                console.log('inside getComplaintCategories error',e);
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        CL.goToViewComplaint = function(complaint){
        	$state.transitionTo('updatecomplaint',{'complaintid':complaint.complaintid}, CONSTANTS.STATE_TRANS_OPTIONS);
        }

        CL.prepareImageGallery = function(complaintdetail){
            /*complaintdetail.complaintimage1 = "assets/img/ums.png";
            complaintdetail.complaintimage2 = "assets/img/hbd-1.jpg";
            complaintdetail.complaintimage3 = "assets/img/social.png";*/
            var images = [
                 
            ];
            if(complaintdetail.complaintimage1 != "" && complaintdetail.complaintimage1 != null){
                    images.push({thumb: complaintdetail.complaintimage1, img: complaintdetail.complaintimage1});
            }
            if(complaintdetail.complaintimage2 != "" && complaintdetail.complaintimage2 != null){
                    images.push({thumb: complaintdetail.complaintimage2, img: complaintdetail.complaintimage2});
            }
            if(complaintdetail.complaintimage3 != "" && complaintdetail.complaintimage3 != null){
                    images.push({thumb: complaintdetail.complaintimage3, img: complaintdetail.complaintimage3});
            }
            CL.complaintImages = images;
        }

        CL.getComplaintDetails = function(){
        	//return;
        	utilityService.showWaitLoader();
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_COMPLAINT_DETAILS;  
                var postData = {'complaintid':$stateParams.complaintid };
                 utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
                	utilityService.hideWaitLoader();
                	console.log("getComplaintDetails success",data);
                	if(data.status == 'OK' && data.complaintlist.length > 0){
                        CL.resolveFormControl = data.complaintlist[0];
                        CL.prepareImageGallery(CL.resolveFormControl);
                    }else{
                        console.log('inside getCastList error',data);
                        utilityService.showAlertMessage(data.message);
                    }
                     
                },function(err){
                	utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
                });
   
            }catch(e){
            	 utilityService.hideWaitLoader();
                console.log('inside getComplaintCategories error',e);
                utilityService.showAlertMessage("Something went wrong please try again");
            }
        }

        CL.getComplaintCategories = function(){
            
            try{
                var postUrl = CONSTANTS.BASE_URL + CONSTANTS.GET_COMPLAINT_CATEGORY;
                utilityService.requestPOSTHttpService(postUrl, {} ,function(data){
                    console.log('inside getComplaintCategories success',data);
                    
                    if(data.status == 'OK' && data.complaintcategory){
                        $scope.complaintcategoryList = data.complaintcategory;
                    }else{
                        console.log('inside getCastList error',data);
                    }
                    
                },function(err){
                    console.log('inside getComplaintCategories error',err);
                    
                });
            }catch(e){
                console.log('inside getComplaintCategories error',e);
            }
        }


        CL.resolveComplaint = function(form){
        	try{
	        	CL.isButtonClicked = true;

	        	if(!form.$valid){
                    $("[name='" + form.$name + "']").find('.ng-invalid:visible:first').focus();
	                return;
	            }

	            var postUrl;
	            postUrl = CONSTANTS.BASE_URL + CONSTANTS.RESOLVE_COMPLAINTS;    
	            utilityService.showWaitLoader();
	            
	            var postData = {
	                            "complaintid":$stateParams.complaintid,
	                            "CID":utilityService.getDataFromLocalStorage('CID') || utilityService.getDataFromLocalStorage('currentCreatedCandidate'),
	                            "resolvedcomments":CL.resolveFormControl.resolvedcomments,
	                            "complaintstatus":CL.resolveFormControl.complaintstatus
	                        };
	            utilityService.requestPOSTHttpService(postUrl, postData ,function(data){
	                console.log('inside resolveComplaint success',data);
	                utilityService.hideWaitLoader();
	                 $scope.isButtonClicked = false;
	                if(data.status == 'OK'){
	                      utilityService.showAlertMessage(data.message);
	                      $state.transitionTo('complaints',{}, CONSTANTS.STATE_TRANS_OPTIONS);

	                }else{
	                    utilityService.hideWaitLoader();
                	    utilityService.showAlertMessage(data.message);
	                }

	                
	                                        
	            },function(err){
	                utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
	            });
			}catch(ex){
				utilityService.hideWaitLoader();
                	utilityService.showAlertMessage("Something went wrong please try again");
			}




        }

        CL.getHeader = function () {
            var arr = [];
            for(var obj in CL.exportComplaintList[0]){
                if(obj != '$$hashKey'){
                    arr.push(obj);
                }
                
            }
            return arr;
        };

        CL.listDateFilterHandler = function(){
                if($("#fromdatepicker" ).val() == ''){
                    alert("कृपया तारीख निवडा");
                    return;
                }
                if( $("#fromdatepicker" ).val() != '' &&  $("#todatepicker" ).val() == ''){
                    alert("कृपया तारीख निवडा");
                    return;
                }
                var fromDate = $( "#fromdatepicker" ).val();
                var toDate = $( "#todatepicker" ).val();
                var dateArray = getDates(new Date(fromDate), new Date(toDate));
                var complaintFileredArray = [];
                for(var i=0 ;i < dateArray.length; i++){
                    var dt = $.datepicker.formatDate( "yy-mm-dd", new Date( dateArray[i] ));
                    var filteredList = _.filter(CL.OriginalComplaintList, function(obj){ 
                        var createdDate = $.datepicker.formatDate( "yy-mm-dd", new Date( obj['created'] ));
                        return (createdDate == dt ); 
                    }); 
                    complaintFileredArray = complaintFileredArray.concat(filteredList);
                }
                CL.myComplaintList = complaintFileredArray;
        };

        CL.$watch('complaintListFilterFormControl.complaintcategory',function(newVal,OldVal){
                if(newVal == undefined){
                    CL.complaintListFilterFormControl.complaintcategory = "";
                }
        });

        CL.removeListFilterHandler = function(){
            CL.complaintListFilterFormControl.filterBy = "";
            CL.complaintListFilterFormControl.complaintcategory = "";
            CL.myComplaintList = CL.OriginalComplaintList;
            //CL.filterCompleted  = false;  
            //$( "#fromdatepicker" ).val('');
            //$( "#todatepicker" ).val('');
            
        }

        CL.listFilterHandler = function(){
        	/*if(!CL.complaintListFilterFormControl.filterBy){
        		 return;
        	}*/
            CL.isSearchedClicked = true;
            /*if( (CL.complaintListFilterFormControl.complaintcategory == '' || CL.complaintListFilterFormControl.complaintcategory == undefined)
                && CL.complaintListFilterFormControl.filterBy == ''){
                CL.removeListFilterHandler();
            }else{*/
                if((CL.complaintListFilterFormControl.complaintcategory != '' )
                    && CL.complaintListFilterFormControl.filterBy == ''){
                    CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                        return (obj['complaintcategory'] ==  CL.complaintListFilterFormControl.complaintcategory ); 
                    });
                }else if(CL.complaintListFilterFormControl.complaintcategory == '' 
                    && CL.complaintListFilterFormControl.filterBy != ''){
                    CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                        return (obj['complaintstatus'] ==  CL.complaintListFilterFormControl.filterBy );  
                    });
                }else if(CL.complaintListFilterFormControl.complaintcategory != '' 
                    && CL.complaintListFilterFormControl.filterBy != ''){
                    if(CL.complaintListFilterFormControl.complaintcategory == undefined){
                         CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                            return (obj['complaintstatus'] ==  CL.complaintListFilterFormControl.filterBy); 
                        });
                    }else{
                        CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                            return (obj['complaintcategory'] ==  CL.complaintListFilterFormControl.complaintcategory 
                                && obj['complaintstatus'] ==  CL.complaintListFilterFormControl.filterBy); 
                        });
                    }
                }else if(CL.complaintListFilterFormControl.complaintcategory == '' 
                    && CL.complaintListFilterFormControl.filterBy == ''){
                        CL.myComplaintList = CL.OriginalComplaintList;
                }  
            //}

            var fromDate = $( "#fromdatepicker" ).val();
            var toDate = $( "#todatepicker" ).val();
            var dateArray = getDates(new Date(fromDate), new Date(toDate));
            var complaintFileredArray = [];
            for(var i=0 ;i < dateArray.length; i++){
                var dt = $.datepicker.formatDate( "yy-mm-dd", new Date( dateArray[i] ));
                var filteredList = _.filter(CL.myComplaintList, function(obj){ 
                    var createdDate = $.datepicker.formatDate( "yy-mm-dd", new Date( obj['created'] ));
                    return (createdDate == dt ); 
                }); 
                complaintFileredArray = complaintFileredArray.concat(filteredList);
            }
            CL.myComplaintList = complaintFileredArray;



            //CL.filterCompleted  = true; 
            /*if(CL.complaintListFilterFormControl.filterBy == 'date'){
                
            }else if(CL.complaintListFilterFormControl.filterBy == 'category'){
                if(CL.complaintListFilterFormControl.complaintcategory == ''){
                    return;
                }
                CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                    return (obj['complaintcategory'] ==  CL.complaintListFilterFormControl.complaintcategory ); 
                });
                if(CL.myComplaintList.length != 0){
                    CL.filterCompleted  = true;   
                    CL.complaintListForDate =  CL.myComplaintList; 
                }
                
            }
            else{
                CL.myComplaintList = _.filter(CL.OriginalComplaintList, function(obj){ 
                    return (obj['complaintstatus'] ==  CL.complaintListFilterFormControl.filterBy ); 
                });
                if(CL.myComplaintList.length != 0){
                    CL.filterCompleted  = true;   
                    CL.complaintListForDate =   CL.myComplaintList;   
                }    
            }*/
        	


        }

        



    }]);


//})(socialApp.controllers);